import Distortion from '../lib/Engine/Distortion'

/** Distortion Spots - position (x, y) and radius (r) */
export const DISTORTIONS: Distortion[] = [
	{x: 0.375, y: 0.53, r: 0.1, a: 0},  // left eye
	{x: 0.625, y: 0.53, r: 0.1, a: 0},  // right eye
	{x: 0.5, y: 0.425, r: 0.075, a: 0}, // nose
	{x: 0.5, y: 0.24, r: 0.125, a: 0},  // mouth
	{x: 0.5, y: 0.8, r: 0.175, a: 0}   // forehead
]

export const VIDEO_WIDTH = 1920 // 4096
export const VIDEO_HEIGHT = 1080 // 2160
export const VIDEO_BITRATE = 2048000

/** Idle timeout after seconds */
export const TIMEOUT_DURATION = 60 * 1000

/** Index of default damera device to use */
export const DEFAULT_CAMERA_INDEX = 0

export const GALLERY_IMAGE_SIZE = 256
