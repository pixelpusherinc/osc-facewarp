import m from 'mithril'
import Socket from '../../../lib/Socket'
import * as svg from '../../../lib/svg'
import {getLang} from '../../lang'
import StartScreen from '../StartScreen'
import Quiz from '../Quiz'
import QuizEnd from '../QuizEnd'
import PhotoBooth from '../PhotoBooth'
import PhotoEditor from '../PhotoEditor'
import EndScreen from '../EndScreen'
import Admin from '../Admin'

let state: 'start' | 'quiz' | 'quizend' | 'photo' | 'edit' | 'end' | 'admin' = 'start'

export default function App(): m.Component {
	let photo: HTMLCanvasElement | undefined
	let socket: Socket

	return {
		oninit: async () => {
			socket = await Socket.create('ws://localhost:3000')
		},
		view: () => m('.app',
			state === 'quiz' ? m(Quiz, {
				onComplete: scores => {
					state = 'quizend'
					socket.send('scores', JSON.stringify(scores))
					socket.send('screen', '2')
				}
			})
			: state === 'quizend' ? m(QuizEnd, {
				onComplete: ok => {
					if (ok) {
						state = 'photo'
						socket.send('screen', '3')
					} else {
						state = 'quiz'
						socket.send('screen', '1')
					}
				}
			})
			: state === 'photo' ? m(PhotoBooth, {
				onComplete: (c: HTMLCanvasElement) => {
					photo = c
					state = 'edit'
					socket.send('screen', '4')
				}
			})
			: state === 'edit' ? m(PhotoEditor, {
				photo,
				onPreview: () => {
					socket.send('screen', '5')
				},
				onComplete: (result) => {
					if (result === 'continue') {
						state = 'end'
						socket.send('screen', '6')
					} else if (result === 'retake') {
						state = 'photo'
						socket.send('screen', '3')
					} else {
						state = 'start'
						socket.send('screen', '0')
					}
				}
			})
			: state === 'end' ? m(EndScreen, {
				onComplete: () => {
					state = 'start'
					socket.send('screen', '0')
				}
			})
			: state === 'admin' ? m(Admin, {
				onComplete: () => {
					state = 'start'
					socket.send('screen', '0')
				}
			})
			: m(StartScreen, {
				onComplete: () => {
					socket.send('lang', getLang())
					socket.send('screen', '1')
					state = 'quiz'
				},
				onAdmin: () => {
					state = 'admin'
				}
			}),
			// Home button on every screen except initial.
			state !== 'start' && m('nav',
				m('button.btn-round',
					{
						type: 'button',
						onclick: () => {
							state = 'start'
							socket.send('screen', '0')
						}
					},
					svg.home()
				)
			)
		)
	}
}
