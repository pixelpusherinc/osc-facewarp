import m from 'mithril'
import Range from 'mithril-range'
import * as svg from '../../../lib/svg'
import Engine from '../../../lib/Engine'
import {DISTORTIONS, GALLERY_IMAGE_SIZE} from '../../config'
import {text} from '../../lang'

const CLEANUP_DELAY = 250

export type PhotoResult = 'cancel' | 'retake' | 'continue'

export interface Attrs {
	photo?: HTMLCanvasElement
	onPreview(): void
	onComplete(result: PhotoResult): void
}

export default function PhotoEditor ({attrs: {onComplete}}: m.Vnode<Attrs>): m.Component<Attrs> {
	// Ensure that we use a fresh copy of distortions
	const distortions = DISTORTIONS.map(d => ({...d}))
	// and filters
	const filters: {id: string, label: string, value: number}[] = [
		{id: 'toon', label: 'Cartoon', value: 0},
		{id: 'vignette', label: 'Vignette', value: 0},
		{id: 'saturation', label: 'Saturate', value: 0},
		{id: 'warm', label: 'Warm', value: 0},
		{id: 'cool', label: 'Cool', value: 0}
	]

	let engine: Engine | undefined
	let confirming = false
	let exiting = false

	function exit(result: PhotoResult) {
		exiting = true
		if (engine != null) {
			engine.dispose()
			engine = undefined
		}
		setTimeout(() => {
			onComplete(result)
			m.redraw()
		}, CLEANUP_DELAY)
	}

	function saveImage() {
		if (engine == null) {
			return Promise.reject(new Error("Can't save image: Engine disposed already!"))
		}
		return engine.createPngBlob(GALLERY_IMAGE_SIZE).then(imgBlob => {
			const data = new FormData()
			//data.append('foo', 'abc')
			data.append('image', imgBlob, 'image.png')
			return m.request<{url: string}>({
				url: '/api/upload',
				method: 'POST',
				body: data
			}).then(
				() => {}
			).catch(err => {
				console.error('Error uploading image: ' + err.message)
				console.error(err)
			})
		}).catch(err => {
			console.error('Error creating blob:', err)
		})
	}

	function savePreview() {
		if (engine == null) {
			return Promise.reject(new Error("Can't save preview: Engine disposed already!"))
		}
		return engine.createPngBlob().then(imgBlob => {
			const data = new FormData()
			//data.append('foo', 'abc')
			data.append('image', imgBlob, 'image.png')
			return m.request<{url: string}>({
				url: '/api/upload-preview',
				method: 'POST',
				body: data
			}).then(
				() => {}
			).catch(err => {
				console.error('Error uploading image: ' + err.message)
				console.error(err)
			})
		}).catch(err => {
			console.error('Error creating blob:', err)
		})
	}

	return {
		oncreate: ({attrs, dom}) => {
			const canvas = dom.querySelector('canvas.photo') as HTMLCanvasElement
			engine = Engine(canvas, distortions,
				Array.from(dom.querySelectorAll('div.distsel')),
				attrs.photo
			)
			engine.ready.then(() => {
				m.redraw()
			}).catch(err => {
				console.warn('Engine failed to initialize: ' + err.message)
				console.error(err)
			})
		},
		onremove: () => {
			if (engine != null) {
				engine.dispose()
				engine = undefined
			}
		},
		view: ({attrs}) => m('.photo-editor',
			m('.photo-container',
				m('canvas.photo', {
					width: 1024, height: 1024
				}),
				distortions.map(() => m('.distsel'))
			),
			m('.editor-controls',
				m('.retake-block',
					m('button.btn-round',
						{
							type: 'button',
							onclick: () => {exit('retake')}
						},
						svg.camera()
					),
					m('span',
						text('retake_photo')
					)
				),
				m('h1.uc', text('warp_tools')),
				distortions.map((dst, i) => m('p',
					m(Range, {
						min: 0, max: 1, step: 0.01,
						value: dst.a,
						ondrag: (a: number) => {
							dst.a = a
							if (engine != null) {
								engine.setDistortionAmount(i, a)
								engine.render()
							}
						},
						onchange: (a: number) => {
							dst.a = a
							if (engine != null) {
								engine.setDistortionAmount(i, a)
								engine.render()
							}
						}
					})
				)),
				m('h1.uc', {style: {margin: '1.25em 0 0.75em 0'}}, text('filters')),
				m('.filter-buttons', filters.map((flt, i) =>
					m('div',
						m('button',
							{
								class: flt.value ? 'active' : '',
								style: {backgroundImage: `url(/img/btn-${i + 1}-${flt.id}.png)`},
								type: 'button',
								onclick: () => {
									flt.value = flt.value === 0 ? 1 : 0
									if (engine != null) {
										engine.setUniform(flt.id, flt.value)
										engine.render()
									}
								}
							},
							' '
						)
					)
				)),
				m('.finish-block',
					m('button.btn-big',
						{
							type: 'button',
							onclick: () => {
								savePreview().then(() => {
									confirming = true
									// Give server an extra moment to ensure we get
									// the current preview image in the viewer.
									setTimeout(() => {
										attrs.onPreview()
									}, 350)
								}).catch(err => {
									console.warn(err.message)
								})
							}
						},
						text('finish')
					)
				)
			),
			confirming && m('.confirm-overlay',
				m('.confirm.block',
					m('h3.fbd.center', text('post_your_face')),
					m('.buttons-row.mt1',
						m('button.btn-big',
							{
								type: 'button',
								onclick: () => {
									saveImage().then(() => {
										exit('continue')
									}).catch(err => {
										console.warn(err.message)
									})
								}
							},
							text('yes')
						),
						m('button.btn-big',
							{
								type: 'button',
								onclick: () => {exit('cancel')}
							},
							text('no')
						)
					)
				)
			),
			exiting && m('.abort-fader')
		)
	}
}
