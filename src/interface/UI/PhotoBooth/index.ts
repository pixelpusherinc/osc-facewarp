import m from 'mithril'
import * as svg from '../../../lib/svg'
import VideoCapture from '../../../lib/VideoCapture'
import {text} from '../../lang'
import {
	VIDEO_WIDTH, VIDEO_HEIGHT, VIDEO_BITRATE, DEFAULT_CAMERA_INDEX
} from '../../config'

/** Fallback video dimensions in case we have trouble detecting from MediaStream */
const FALLBACK = {width: 1280, height: 720}

export interface Attrs {
	onComplete (canvas: HTMLCanvasElement): void
}

export default function PhotoBooth(): m.Component<Attrs> {
	let mediaStream: MediaStream | undefined
	let requestingStream = false
	let takingPicture = false
	let video: (HTMLVideoElement & {srcObject: any}) | null | undefined
	const videoDimensions = {...FALLBACK}
	const vcap = VideoCapture({
		width: VIDEO_WIDTH, height: VIDEO_HEIGHT, bitrate: VIDEO_BITRATE
	})
	let devices: MediaDeviceInfo[] = []
	vcap.ready.then(devs => {
		devices = devs
		if (devices.length < 1) {
			console.warn('No video input devices found!')
			return
		}
		Promise.resolve().then(() => {
			const index = Math.min(DEFAULT_CAMERA_INDEX, devices.length - 1)
			startCapture(index)
		})
	})

	/** Start capturing video stream from device by array index */
	function startCapture (index: number) {
		if (requestingStream || mediaStream) {
			console.log('Capture already started')
			return
		}
		requestingStream = true
		const device = devices[index]
		console.log('Using camera:', device.label)
		vcap.start(device.deviceId).then(mstr => {
			requestingStream = false
			mediaStream = mstr
			const tracks = mediaStream.getTracks()
			if (tracks.length > 0) {
				if (tracks.length > 1) {
					console.warn('Found more than one track in media stream. Defaulting to first track.')
				}
				const trk = tracks[0]
				const s = trk.getSettings()
				if (s.width && s.height) {
					videoDimensions.width = s.width
					videoDimensions.height = s.height
				} else {
					console.warn('MediaTackSettings had undefined width/height:', s)
				}
				console.log(`Video dimensions: ${videoDimensions.width} x ${videoDimensions.height}`)
			} else {
				console.warn('Found no tracks in MediaStream!')
			}
			video!.srcObject = mediaStream
			m.redraw()
		}).catch(err => {
			console.error('Error initializing video capture: ' + err.message)
		})
	}

	function savePhoto (canvas: HTMLCanvasElement) {
		return new Promise<Blob>((resolve, reject) =>
			canvas.toBlob(blob =>
				blob ? resolve(blob) : reject(new Error('Failed to create blob'))
			)
		).then(blob => {
			const data = new FormData()
			data.append('image', blob, 'image.png')
			return m.request<{url: string}>({
				url: '/api/temp-upload',
				method: 'POST',
				body: data
			}).then(
				() => {}
			).catch(err => {
				console.error('Error uploading photo: ' + err.message)
				console.error(err)
			})
		}).catch(err => {
			console.error('Error saving photo blob:', err)
		})
	}

	function takePhoto() {
		// Crop to a square within the video rectangle
		const vSize = videoDimensions.height
		const tSize = vSize > 1280 ? 2048 : 1024
		console.log(`Using texture size: ${tSize}x${tSize}`)
		const xpad = Math.floor((videoDimensions.width - videoDimensions.height) / 2)
		const canvas = document.createElement('canvas')
		canvas.width = tSize
		canvas.height = tSize
		const ctx = canvas.getContext('2d')!
		ctx.drawImage(video!,
			xpad, 0, vSize, vSize,
			0, 0, tSize, tSize
		)
		return canvas
	}

	function resize() {
		if (video == null) return
		const height = video.getBoundingClientRect().height
		video.style.width = `${height}px`
	}

	return {
		oncreate: ({dom}) => {
			if (!(video = dom.querySelector('video'))) { // tslint:disable-line no-conditional-assignment
				console.warn('Video element not found!')
				return
			}
			window.addEventListener('resize', resize)
			resize()
		},
		onremove: () => {
			window.removeEventListener('resize', resize)
			vcap.stop()
			mediaStream = undefined
			devices = []
			video = undefined
		},
		view: ({attrs: {onComplete}}) => m('.photo-booth',
			m('.video-container',
				m('video', {
					autoplay: true
				}),
				// m('.capture-start',
				// 	{style: 'position: absolute; left: 2em; top: 2em;'},
				// 	devices.map((dev, i) => m('p', m('button',
				// 		{
				// 			key: dev.deviceId,
				// 			type: 'button',
				// 			disabled: !!mediaStream || requestingStream,
				// 			onclick: () => {startCapture(i)}
				// 		},
				// 		(mediaStream ? 'Capturing ' : 'Capture ') +  dev.label
				// 	)))
				// )
			),
			m('.camera-controls',
				// m('.step',
				// 	m('.yellow.uc.fbd', text('step') + ' 1'),
				// 	m('div', text('step1'))
				// ),
				m('.step',
					m('.yellow.uc.fbd', text('step') + ' 1'),
					m('div', text('step2'))
				),
				m('.step',
					m('.yellow.uc.fbd', text('step') + ' 2'),
					m('div', text('step3'))
				),
				m('.shoot-container',
					m('button.btn-round',
						{
							type: 'button',
							disabled: takingPicture || !mediaStream,
							onclick: () => {
								if (!takingPicture) {
									takingPicture = true
									const canvas = takePhoto()
									savePhoto(canvas).then(() => {
										onComplete(canvas)
									})
								}
							}
						},
						svg.camera()
					)
				)
			)
		)
	}
}
