import m from 'mithril'
import {text} from '../../lang'

export interface Attrs {
	onComplete(): void
}

export default function EndScreen(): m.Component<Attrs> {
	return {
		view: ({attrs: {onComplete}}) => m('.start-screen',
			m('button.btn-big',
				{
					onclick: onComplete
				},
				text('restart')
			)
		)
	}
}
