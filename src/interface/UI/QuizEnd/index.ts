import m from 'mithril'
import {text} from '../../lang'

export interface Attrs {
	onComplete(ok: boolean): void
}

const QuizEnd: m.Component<Attrs> = {
	view: ({attrs: {onComplete}}) => m('.quiz-end',
		m('.buttons-row',
			m('button.btn-big',
				{
					type: 'button',
					onclick: () => {onComplete(true)}
				},
				text('continue')
			),
			m('button.btn-big',
				{
					type: 'button',
					onclick: () => {onComplete(false)}
				},
				text('redo_test')
			)
		)
	)
}

export default QuizEnd
