import m from 'mithril'
import {EN, FR, setLang} from '../../lang'
import * as fullscreen from '../../../lib/fullscreen'

const ADMIN_CLICKS_REQUIRED = 5

let isFullscreen = (function() {
	const params = m.parseQueryString(window.location.search)
	return !!params.fullscreen
}())

export interface Attrs {
	onComplete(): void
	onAdmin(): void
}

export default function StartScreen(): m.Component<Attrs> {
	let adminClicks = 0

	return {
		view: ({attrs: {onComplete, onAdmin}}) => m('.start-screen',
			m('div',
				m('.lang-select-text',
					{style: {textAlign: 'center', marginBottom: '1em'}},
					m('div', EN.touch_to_start),
					m('div', FR.touch_to_start)
				),
				m('.buttons-row',
					m('button.btn-big',
						{
							onclick: () => {
								setLang('en')
								onComplete()
							}
						},
						'English'
					),
					m('button.btn-big',
						{
							onclick: () => {
								setLang('fr')
								onComplete()
							}
						},
						'Français'
					)
				),
				!isFullscreen && m('.config-ui',
					m('button',
						{
							type: 'button',
							onclick: () => {
								fullscreen.toggle(document.body)
								isFullscreen = true
							}
						},
						'Fullscreen'
					)
				),
				m('.admin-trigger', {
					onclick: () => {
						adminClicks += 1
						if (adminClicks >= ADMIN_CLICKS_REQUIRED) {
							// Go to admin screen
							onAdmin()
						}
					}
				})
			)
		)
	}
}
