import m from 'mithril'

const COLS = 5
const ROWS = 4

export interface Attrs {
	onComplete(): void
}

export default function Admin(): m.Component<Attrs> {
	let selectedIndex: number | undefined
	let imgUrls: string[] = []
	let busy = false

	function loadImages() {
		return m.request<{files: string[]}>({
			url: '/api/list'
		}).then(result => {
			imgUrls = result.files
		}).catch(err => {
			console.error('Failed to load gallery images')
			console.error(err.stack)
		})
	}

	function deleteImage (index: number) {
		const url = imgUrls[index]
		const timestamp = timestampFromUrl(url)
		if (!timestamp) {
			console.warn('Could not parse timestamp from url:', url)
		}
		busy = true
		return m.request({
			url: '/api/delete',
			method: 'POST',
			body: {timestamp}
		}).catch(err => {
			busy = false
			console.error('Failed to delete image:', err)
		}).then(
			() => loadImages()
		).then(
			() => {busy = false}
		)
	}

	return {
		oninit: () => {
			loadImages()
		},
		view: ({attrs: {onComplete}}) => m('.admin',
			m('admin-ctrls',
				m('h2.center.uc', 'Administration'),
				m('.images-grid',
					m('table',
						Array.from({length: ROWS}, (_, r) => m('tr',
							Array.from({length: COLS}, (_, c) => {
								const index = r * COLS + c
								if (index >= imgUrls.length || !imgUrls[index]) {
									return m('td', m('.empty-cell', ' '))
								}
								const imgUrl = imgUrls[index]
								return m('td',
									m('button.btn-img',
										{
											type: 'button',
											class: index === selectedIndex ? 'selected' : '',
											style: {backgroundImage: `url(${imgUrl})`},
											onclick: () => {
												selectedIndex = selectedIndex === index ? undefined : index
											}
										},
										' '
									)
								)
							})
						))
					)
				),
				m('.buttons-row.mt1.center',
					m('button',
						{
							type: 'button',
							disabled: busy || selectedIndex == null,
							onclick: () => {
								if (selectedIndex != null) {
									deleteImage(selectedIndex).then(() => {
										selectedIndex = undefined
									})
								}
							}
						},
						'Delete Selected'
					),
					m('button',
						{
							type: 'button',
							onclick: () => {onComplete()}
						},
						'Exit'
					)
				)
			)
		)
	}
}

const RX_IMGURL = /([0-9]{10})\.png$/

function timestampFromUrl (url: string) {
	const r = RX_IMGURL.exec(url)
	if (!r || !r[1]) {
		return undefined
	}
	return r[1]
}
