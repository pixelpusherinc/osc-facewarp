import m from 'mithril'
import {text, TextKey} from '../../lang'
import Answer from './Answer'
import {QUESTIONS} from './data'

export interface Attrs {
	qIndex: number
	answer: Answer
	onAnswer(answer: Answer): void
}

const answerButtons: {key: TextKey}[] = [
	{key: 'a_lot'},
	{key: 'a_little'},
	{key: 'both_depends'},
	{key: 'a_little'},
	{key: 'a_lot'}
]

const QuizRow: m.Component<Attrs> = {
	view: ({attrs: {qIndex, answer, onAnswer}}) => {
		const qitem = QUESTIONS[qIndex]
		const id = String(qIndex + 1).padStart(2, '0')
		return m('tr.content',
			m('td',
				m('img.img-quiz', {src: `/img/quiz/q${id}a.png`})
			),
			m('td',
				m('.quiz-item',
					m('h3', text('i_see_myself')),
					m('.more-less',
						m('.more', text(qitem.more)),
						m('.less', text(qitem.less))
					),
					m('.answers', answerButtons.map(
						(ab, i) => m('button.btn-quiz',
							{
								class: answer === i + 1 ? 'selected' : '',
								type: 'button',
								onclick: () => {
									onAnswer(i + 1 as Answer)
								}
							},
							text(ab.key)
						)
					))
				)
			),
			m('td',
				m('img.img-quiz', {src: `/img/quiz/q${id}b.png`})
			)
		)
	}
}

export default QuizRow
