import m from 'mithril'
import {OceanScores} from '../../../lib/ocean'
import {text} from '../../lang'
import {QUESTIONS} from './data'
import Answer from './Answer'
import QuizRow from './QuizRow'

export interface Attrs {
	onComplete(scores: OceanScores): void
}

export default function Quiz(): m.Component<Attrs> {
	const answers: Answer[] = QUESTIONS.map(q => undefined)

	function computeScores(): OceanScores | undefined {
		if (answers.some(a => a == null)) {
			return undefined
		}
		const scores: OceanScores = {
			O: 0, C: 0, E: 0, A: 0, N: 0
		}
		for (let i = 0; i < answers.length; ++i) {
			const a = answers[i]!
			const q = QUESTIONS[i]
			scores[q.trait] += q.sign > 0 ? a : 6 - a
		}
		for (const k of Object.keys(scores) as (keyof OceanScores)[]) {
			scores[k] /= 2
		}
		return scores
	}

	return {
		view: ({attrs}) => m('.quiz',
			m('table.tbl-quiz', QUESTIONS.map((_, i) =>
				[
					m(QuizRow, {
						qIndex: i,
						answer: answers[i],
						onAnswer: (a: Answer) => {
							answers[i] = a
						}
					}),
					i < QUESTIONS.length - 1 && m('tr.spacer',
						m('td', {colspan: 3}, m.trust('&nbsp;'))
					)
				]
			)),
			m('.quiz-footer',
				m('button.btn-round',
					{
						type: 'button',
						disabled: answers.some(a => a == null),
						onclick: () => {
							const scores = computeScores()
							if (scores) {
								console.log('scores:', scores)
								attrs.onComplete(scores)
							}
						}
					},
					text('submit')
				)
			)
		)
	}
}
