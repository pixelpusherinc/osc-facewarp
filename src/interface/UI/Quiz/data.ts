import {TextKey} from '../../lang'
import {PersonalityTrait, TraitSign} from '../../../lib/ocean'

export interface QuizQuestion {
	more: TextKey
	less: TextKey
	trait: PersonalityTrait
	sign: TraitSign
}

export const QUESTIONS: QuizQuestion[] = [
	{
		more: 'likes_to_talk',
		less: 'likes_to_stay_silent',
		trait: 'E',
		sign: -1
	},
	{
		more: 'easily_worried',
		less: 'easy_going',
		trait: 'N',
		sign: -1
	},
	{
		more: 'quick_to_argue',
		less: 'quick_to_forgive',
		trait: 'A',
		sign: 1
	},
	{
		more: 'likes_new_ideas',
		less: 'likes_familiar_ideas',
		trait: 'O',
		sign: -1
	},
	{
		more: 'keeps_things_neat',
		less: 'keeps_things_messy',
		trait: 'C',
		sign: -1
	},
	{
		more: 'plays_alone',
		less: 'plays_with_others',
		trait: 'E',
		sign: 1
	},
	{
		more: 'kind_to_others',
		less: 'rude_to_others',
		trait: 'A',
		sign: -1
	},
	{
		more: 'likes_to_relax',
		less: 'likes_to_keep_busy',
		trait: 'C',
		sign: 1
	},
	{
		more: 'stress_well',
		less: 'stress_poorly',
		trait: 'N',
		sign: 1
	},
	{
		more: 'stick_to_one_thing',
		less: 'do_different_things',
		trait: 'O',
		sign: 1
	}
]
