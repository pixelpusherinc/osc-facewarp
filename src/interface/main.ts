import m from 'mithril'
import App from './UI/App'

// Init the App by mounting the root Mithril component
m.mount(document.body as HTMLElement, App)
