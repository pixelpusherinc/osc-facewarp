// EN & FR text content

export type Language = 'en' | 'fr'
export type TextDict = typeof EN
export type TextKey = keyof typeof EN

export const EN = {
	yes: 'Yes',
	no: 'No',
	submit: 'Submit',
	step: 'Step',

	continue: 'Continue',
	redo_test: 'Redo Test',
	restart: 'Restart',

	lang_select: 'Language select',
	touch_to_start: 'Touch to explore your personality.',

	step1: 'Position your eyes within the guides.',
	step2: 'Look into the camera above this screen.',
	step3: 'Take the photo.',

	retake_photo: 'Retake Photo',
	warp_tools: 'Warp Tools',
	filters: 'Filters',
	cartoon: 'Cartoon',
	vignette: 'Vignette',
	saturate: 'Saturate',
	warm: 'Warm',
	cool: 'Cool',
	finish: 'Finish',

	post_your_face: 'Post your face in the image gallery?',

	describe_yourself: 'How would you describe yourself? Touch the answer that best suits you.',

	i_see_myself: 'I see myself as someone who...',
	a_lot: 'A lot',
	a_little: 'A little',
	both_depends: `Both
(it depends)`,

	likes_to_talk: 'likes to talk',
	likes_to_stay_silent: 'likes to stay silent',

	easily_worried: 'is easily worried',
	easy_going: 'is easy going',

	quick_to_argue: 'is quick to argue',
	quick_to_forgive: 'is quick to forgive',

	likes_new_ideas: 'likes new ideas',
	likes_familiar_ideas: 'likes familiar ideas',

	keeps_things_neat: 'keeps things neat',
	keeps_things_messy: 'keeps things messy',

	plays_alone: 'likes to play alone',
	plays_with_others: 'likes to play with others',

	kind_to_others: 'is kind to others',
	rude_to_others: 'is rude to others',

	likes_to_relax: 'likes to relax',
	likes_to_keep_busy: 'likes to keep busy',

	stress_well: 'handles stress well',
	stress_poorly: 'handles stress poorly',

	stick_to_one_thing: 'likes to stick to one thing',
	do_different_things: 'likes to do different things',
}

export const FR: TextDict = {
	yes: 'Oui',
	no: 'Non',
	submit: 'Envoyez',
	step: 'ÉTAPE',

	continue: 'Continuer',
	redo_test: 'Recommencer',
	restart: 'Départ',

	lang_select: 'Sélection de la langue',
	touch_to_start: 'Touchez pour explorer votre personnalité.',

	step1: 'Vos yeux comme sur l’image.',
	step2: 'Regardez l’objectif.',
	step3: 'Prendre la photo',

	retake_photo: 'Reprendre la photo',
	warp_tools: 'Modifications',
	filters: 'Filtres',
	cartoon: 'Cartoon',
	vignette: 'Vignette',
	saturate: 'Saturate',
	warm: 'Warm',
	cool: 'Cool',
	finish: 'Fin',

	post_your_face: 'Post your face in the image gallery?',

	describe_yourself: 'Quelle description vous ressemble le plus? Touchez la meilleure réponse.',

	i_see_myself: 'Je pense être quelqu’un qui...',
	a_lot: 'souvent',
	a_little: 'un peu',
	both_depends: 'les deux… ça dépend',

	likes_to_talk: 'aime parler',
	likes_to_stay_silent: 'aime se taire',

	easily_worried: 's’inquiète vite',
	easy_going: 'se rassure vite',

	quick_to_argue: 'se dispute pour un rien ',
	quick_to_forgive: 'se réconcilie vite',

	likes_new_ideas: 'aime les nouvelles idées',
	likes_familiar_ideas: 'aime les idées connues',

	keeps_things_neat: 'met de l’ordre',
	keeps_things_messy: 'met du désordre',

	plays_alone: 'joue mieux en solitaire',
	plays_with_others: 'joue mieux avec d’autres',

	kind_to_others: 'traite les autres gentiment',
	rude_to_others: 'traite les autres méchamment',

	likes_to_relax: 'aime relaxer',
	likes_to_keep_busy: 'aime l’activité',

	stress_well: 'supporte bien le stress',
	stress_poorly: 'supporte mal le stress',

	stick_to_one_thing: 'aime faire une seule chose',
	do_different_things: 'aime faire plusieurs choses',
}

let curLang: Language = 'en'
let curDict: TextDict = EN

export function getLang(): Language {
	return curLang
}

export function setLang (lang: Language) {
	curLang = lang
	curDict = lang === 'fr' ? FR : EN
}

/** Returns the EN or FR text for the current Language */
export function text (key: TextKey) {
	return curDict[key]
}
