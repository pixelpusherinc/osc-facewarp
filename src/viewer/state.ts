import m from 'mithril'
import {OceanScores} from '../lib/ocean'
import {MAX_IMAGES} from './config'

const _state = { // tslint:disable-line variable-name
	images: Array.from({length: MAX_IMAGES}, () => '/img/gallery-default.png'),
	scores: undefined as OceanScores | undefined
}

export const state: Readonly<typeof _state> = _state

export const actions = {
	setScores: (scores: OceanScores | undefined) => {
		_state.scores = scores
	},
	loadImages: async () => {
		try {
			const result = await m.request<{files: string[]}>({
				url: '/api/list'
			})
			_state.images = result.files
			// If we didn't get 20, use placeholders
			if (_state.images.length < MAX_IMAGES) {
				const result2 = await m.request<{files: string[]}>({
					url: '/api/placeholders'
				})
				// If we didn't find any placeholders, fall back on this one image
				const pimgs = result2.files.length > 0 ? result2.files : ['/img/gallery-default.png']
				// Now fill (and repeat if necessary) until we have MAX_IMAGES
				for (let i = 0; _state.images.length < MAX_IMAGES; ++i) {
					_state.images.push(pimgs[i % pimgs.length])
				}
			}
			return _state.images
		} catch (err) {
			console.error('Failed to load gallery images:', err.message)
			return _state.images
		}
	},
	cleanImages: () => m.request({
		url: '/api/clean-old',
		method: 'POST'
	}).catch(err => {
		console.error('Failed to clean old images:', err.message)
	})
}
