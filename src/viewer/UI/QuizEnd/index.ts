import m from 'mithril'
import {PersonalityTrait, scoreToString} from '../../../lib/ocean'
import {GALLERY_COLS} from '../../config'
import {state} from '../../state'
import {text, getLang} from '../../lang'
import Gallery from '../Gallery'

const ROWS = 2

export default function Quiz() {
	const lang = getLang()
	return [
		Gallery(ROWS, GALLERY_COLS),
		state.scores && m('.quiz-end',
			m('h1.lbr',
				text('based_on_answers')
			),
			m('.personality-traits',
				(Object.keys(state.scores) as PersonalityTrait[]).map(t =>
					m('.trait', scoreToString(state.scores!, t, lang))
				)
			)
		)
	]
}
