import m from 'mithril'
import {GALLERY_COLS} from '../../config'
import {text} from '../../lang'
import Gallery from '../Gallery'

const ROWS = 2

export default function Quiz() {
	return [
		Gallery(ROWS, GALLERY_COLS),
		m('.quiz-prompt',
			m('h1.uc.lbr', text('how_describe_yourself')),
			m('h3', text('touch_the_answer'))
		)
	]
}
