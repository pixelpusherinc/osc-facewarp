import m from 'mithril'
import {text} from '../../lang'
import Gallery from '../Gallery'

const ROWS = 2
const COLS = 4

/** Screen that displays while photo is being taken */
export default function PosePhoto() {
	return [
		Gallery(ROWS, COLS),
		m('.pose-photo',
			m('h1', text('face_express')),
			m('.bottom-block',
				m('h3', text('pose_picture')),
				m('.icon-block',
					m('.icon-circle', svgArrow())
				)
			)
		)
	]
}

/** Renders SVG arrow */
function svgArrow() {
	return m('svg.arrow',
		{viewBox: '0 0 106.12 147.42'},
		m('path', {
			d: 'M53.06 147.42L0 94.36V67.89l43.69 43.69V0h18.74v111.58l43.69-43.69v26.47z'
		})
	)
}
