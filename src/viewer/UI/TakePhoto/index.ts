import m from 'mithril'
import {text} from '../../lang'
import Gallery from '../Gallery'

const ROWS = 2
const COLS = 4

export default function TakePhoto() {
	return [
		Gallery(ROWS, COLS),
		m('.content-block',
			m('h1', text('pose_picture'))
		)
	]
}
