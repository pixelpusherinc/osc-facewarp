import m from 'mithril'
import {GALLERY_COLS} from '../../config'
import {text} from '../../lang'
import Gallery from '../Gallery'

const ROWS = 2

export default function Quiz() {
	return [
		Gallery(ROWS, GALLERY_COLS),
		m('.finale',
			m('h1', text('reflect_personality')),
			m('h1', text('normally_reveal'))
		)
	]
}
