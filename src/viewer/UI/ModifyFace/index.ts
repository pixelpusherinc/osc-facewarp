import m from 'mithril'
import {buildResultString, text} from '../../lang'
import {state} from '../../state'

let cacheBuster = createCacheBuster()

export default function ModifyFace() {
	return [
		m('.modify-face',
			m('h2.lbr', text('based_on_answers')),
			state.scores && m('h1', buildResultString(state.scores)),
			m('h3', text('modify_face'))
		),
		m('.bottom-photo',
			m('img.img-photo', {
				oninit: () => {cacheBuster = createCacheBuster()},
				src: '/upload/temp.png?q=' + cacheBuster
			})
		)
	]
}

function createCacheBuster() {
	return String(Math.floor(Date.now() / 100))
}
