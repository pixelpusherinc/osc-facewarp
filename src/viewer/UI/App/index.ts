import m from 'mithril'
import {pmod} from '../../../lib/math'
import * as fullscreen from '../../../lib/fullscreen'
import Socket from '../../../lib/Socket'
import {OceanScores} from '../../../lib/ocean'
import {actions} from '../../state'
import {setLang} from '../../lang'
import Home from '../Home'
import Quiz from '../Quiz'
import QuizEnd from '../QuizEnd'
import PosePhoto from '../PosePhoto'
import ModifyFace from '../ModifyFace'
import PreviewImage from '../PreviewImage'
import Finale from '../Finale'

const SCREENS = [
	Home,         // 0
	Quiz,         // 1
	QuizEnd,      // 2
	PosePhoto,    // 3
	ModifyFace,   // 4
	PreviewImage, // 5
	Finale        // 6
]

const REFRESH_DURATION = 5 * 60 * 1000

/** Current screen index */
let screen = 0

export default function App() {
	function refreshLoop() {
		if (screen === 0) {
			actions.cleanImages().then(
				() => actions.loadImages()
			).catch(err => {
				console.warn('Error refreshing image list:', err)
			})
		}
		setTimeout(refreshLoop, REFRESH_DURATION)
	}

	/** Handle a goto screen socket message. Data should be the screen index. */
	function onGotoScreen (data: string | undefined) {
		const i = Number(data)
		if (!Number.isSafeInteger(i) || i < 0 || i >= SCREENS.length) {
			console.warn(`Received invalid screen index from socket: '${data}'`)
			return
		}
		screen = i
		if (screen === 0 || screen === SCREENS.length - 1) {
			// Re-load latest gallery images whenever we go to finale or home screen
			actions.loadImages()
		} else {
			m.redraw()
		}
	}

	return {
		oninit: async () => {
			// Clean old & load existing gallery images on startup
			await actions.cleanImages()
			await actions.loadImages()
			const socket = await Socket.create('ws://localhost:3000')
			socket.on('screen', onGotoScreen)
			socket.on('lang', l => {
				if (l !== 'en' && l !== 'fr') {
					console.warn(`Got unexpected lang message from socket: '${l}'`)
					return
				}
				setLang(l)
				m.redraw()
			})
			socket.on('scores', json => {
				try {
					actions.setScores(JSON.parse(json!) as OceanScores)
				} catch (err) {
					console.warn('Failed to parse scores message:', err)
				}
			})
			// Kickstart our refresh loop
			setTimeout(refreshLoop, REFRESH_DURATION)
		},
		view: () => m('.app',
			SCREENS[screen]()
			//!fullscreen.is() && Nav()
		)
	}
}

// For development

function Nav() {
	return m('.nav',
		m('.prev',
			m('button',
				{
					type: 'button',
					onclick: () => {
						screen = pmod(screen - 1, SCREENS.length)
					}
				},
				'< prev'
			)
		),
		m('.next',
			m('button',
				{
					type: 'button',
					onclick: () => {
						screen = pmod(screen + 1, SCREENS.length)
					}
				},
				'next >'
			)
		)
	)
}
