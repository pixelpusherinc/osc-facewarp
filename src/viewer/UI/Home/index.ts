import m from 'mithril'
import * as fullscreen from '../../../lib/fullscreen'
import {GALLERY_COLS} from '../../config'
import {EN, FR} from '../../lang'
import Gallery from '../Gallery'

let isFullscreen = (function() {
	const params = m.parseQueryString(window.location.search)
	return !!params.fullscreen
}())

const ROWS = 5

export default function Home() {
	return [
		m('.header',
			m('h1', EN.who_are_you),
			m('h1', FR.who_are_you)
		),
		Gallery(ROWS, GALLERY_COLS),
		!isFullscreen && m('.config-ui',
			m('button',
				{
					type: 'button',
					onclick: () => {
						fullscreen.toggle(document.body)
						isFullscreen = true
					}
				},
				'Fullscreen'
			)
		)
	]
}
