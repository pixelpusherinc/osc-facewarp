import m from 'mithril'

import {state} from '../../state'

export default function Gallery (rows: number, cols: number) {
	return m('.gallery',
		m('table.tbl-gallery',
			Array.from({length: rows}, (_1, row) => m('tr',
				Array.from({length: cols}, (_2, col) => m('td',
					m('img', {
						src: state.images[row * cols + col]
							|| 'img/gallery-default.png'
					})
				))
			))
		)
	)
}
