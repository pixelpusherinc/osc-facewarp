import m from 'mithril'
import {DEFAULT_WIDTH, DEFAULT_FONT_SIZE} from './config'
import App from './UI/App'

// Init the App by mounting the root Mithril component
m.mount(document.body as HTMLElement, App)

function resize() {
	const w = window.innerWidth
	const scale = Math.min(w / DEFAULT_WIDTH, 1.0)
	document.body.style.fontSize = (DEFAULT_FONT_SIZE * scale) + 'px'
}

window.addEventListener('resize', resize)
resize()
