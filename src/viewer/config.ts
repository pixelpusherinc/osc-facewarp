export const DEFAULT_WIDTH = 1080

export const DEFAULT_FONT_SIZE = 32

export const MAX_IMAGES = 20

export const GALLERY_COLS = 4
