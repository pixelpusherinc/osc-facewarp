import {PersonalityTrait, OceanScores, scoreToString} from '../lib/ocean'

// EN & FR text content

export type Language = 'en' | 'fr'
export type TextDict = typeof EN
export type TextKey = keyof typeof EN

export const EN = {
	and: 'and',

	who_are_you: `Who do you
think you are?`,
	press_start_to_begin: 'Press start to begin exploring your personality',

	instructions: 'Instructions',
	how_describe_yourself: `How would you
describe yourself?`,
	touch_the_answer: 'Touch the answer that best suits you.',

	based_on_answers: `Based on your answers,
you have described yourself as`,

	p_conventional: 'Conventional',
	p_hardworking: 'Hardworking',
	p_reserved: 'Reserved',
	p_critical: 'Critical',
	p_calm: 'Calm',

	face_express: `Can your face express
your personality?`,
	pose_picture: 'Pose for a picture.',

	modify_face: `Modify your face to show off
your personality`,

	reflect_personality: 'How hard was it to reflect your personality on your face?',
	normally_reveal: 'How do you normally reveal your inner self?'
}

export const FR: TextDict = {
	and: 'et',

	who_are_you: `Que pensez-vous
de vous?`,
	press_start_to_begin: 'Appuyez sur « Départ » pour explorer votre personnalité.',

	instructions: 'Instructions',
	how_describe_yourself: `Quelle description
vous ressemble le plus?`,
	touch_the_answer: 'Touchez la meilleure réponse.',

	based_on_answers: `D’après vos réponses,
	votre personnalité est`,

	p_conventional: '*FR*Conventional*FR*',
	p_hardworking: '*FR*Hardworking*FR*',
	p_reserved: '*FR*Reserved*FR*',
	p_critical: '*FR*Critical*FR*',
	p_calm: '*FR*Calm*FR*',

	face_express: `Faites une grimace qui
montre votre personnalité!`,
	pose_picture: 'Prenez la pose.',

	modify_face: `Changez votre photo pour montrer votre personnalité.`,

	reflect_personality: 'Trouvez-vous difficile de montrer votre personnalité dans une grimace?',
	normally_reveal: 'Comment révélez-vous votre intérieur d’habitude?'
}

let curLang: Language = 'en'
let curDict: TextDict = EN

export function getLang(): Language {
	return curLang
}

export function setLang (lang: Language) {
	curLang = lang
	curDict = lang === 'fr' ? FR : EN
}

/** Returns the EN or FR text for the current Language */
export function text (key: TextKey) {
	return curDict[key]
}

/** Builds personality results string */
export function buildResultString (scores: OceanScores) {
	const lang = getLang()
	const keys = Object.keys(scores) as PersonalityTrait[]
	let str = ''
	// Concat with commas
	for (let i = 0; i < keys.length - 1; ++i) {
		if (i > 0) str += ', '
		str += scoreToString(scores, keys[i], lang).toUpperCase()
	}
	// and last one
	str += ` ${text('and')} ${scoreToString(scores, keys[keys.length - 1], lang).toUpperCase()}`
	return str
}
