// DOM utils

export function getCssRule (sel: string) {
	const rules: any[] = (window.document.styleSheets[0] as any)['cssRules']
		|| (window.document.styleSheets[0] as any)['rules']
	for (let i = 0, n = rules.length; i < n; ++i) {
		if (rules[i].selectorText === sel) return rules[i]
	}
	return undefined
}

/**
 * Eg: setStyle('.title', 'fontSize', '24px');
 */
export function setCssStyle (sel: string, style: string, val: string) {
	const rule = getCssRule(sel)
	if (!rule) return
	rule.style[style] = val
}

/**
 * Ensures this DOM element is rendered and ready so that CSS animations can be applied.
 */
export function readyDom (dom: Element) {
	// Assign value to a variable for side-effects.
	// Reading from the DOM element ensures it is rendered.
	let temp = (dom as HTMLElement).offsetHeight /* tslint:disable-line */
}

export interface IEvent extends Event {
	currentTarget: HTMLInputElement
}
