import V2 from '../../lib/vec/V2'

interface Distortion {
	x: number
	y: number
	r: number
	a: number
}

namespace Distortion {
	export function nearestWithin (p: V2, ds: Distortion[]): number | undefined {
		let minDistance: number | undefined
		let index: number | undefined
		for (let i = 0; i < ds.length; ++i) {
			const d = ds[i]
			const distance = V2.dist(p, d)
			if (V2.dist(p, d) < d.r && (minDistance == null || distance < minDistance)) {
				index = i
				minDistance = distance
			}
		}
		return index
	}
}

export default Distortion
