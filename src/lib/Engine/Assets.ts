import * as THREE from 'three'
import {loadTexture, loadText} from '../../lib/loaders'
import Distortion from './Distortion'

function createPlaneGeo() {
	const g = new THREE.BufferGeometry()
	g.addAttribute('aPosition', new THREE.BufferAttribute(new Float32Array([
		-1, -1,
		1, -1,
		1, 1,
		-1, 1
	]), 2))
	g.addAttribute('aUv', new THREE.BufferAttribute(new Float32Array([
		0, 0,
		1, 0,
		1, 1,
		0, 1
	]), 2))
	g.setIndex([0, 1, 2, 2, 3, 0])
	return g
}

interface Assets {
	planeGeo: THREE.BufferGeometry
	planeMat: THREE.RawShaderMaterial
	dispose(): void
}

function Assets (distortions: Distortion[], photo?: HTMLCanvasElement): Promise<Assets> {
	let planeGeo: THREE.BufferGeometry | undefined
	let planeTex: THREE.Texture | undefined
	let planeMat: THREE.RawShaderMaterial | undefined

	/* Create 2D plane with custom shader material & texture */
	async function createPlaneMat() {
		if (planeTex != null) {
			planeTex.dispose()
			planeTex = undefined
		}
		const [texture, vsrc, fsrc] = await Promise.all([
			photo ? Promise.resolve(new THREE.CanvasTexture(photo)) : loadTexture('img/texture/face-01-girl.jpg'),
			loadText('shader/toon.vertex.glsl'),
			loadText('shader/toon.fragment.glsl')
		])
		planeTex = texture
		return new THREE.RawShaderMaterial({
			uniforms: {
				uSpots: {value: distortions.map(d => new THREE.Vector4(d.x, d.y, d.r, 0))},
				uToon: {value: 0},
				uVignette: {value: 0},
				uSaturation: {value: 0},
				uWarm: {value: 0},
				uCool: {value: 0},
				uViewport: {value: new THREE.Vector2(1, 1)},
				uTexture: {value: texture}
			},
			vertexShader: vsrc,
			fragmentShader: fsrc.replace(
				/\{\{NUM_SPOTS\}\}/g, String(distortions.length)
			),
			depthTest: false
		})
	}

	function dispose() {
		if (planeMat != null) {
			planeMat.dispose()
			planeMat = undefined
		}
		if (planeTex != null) {
			planeTex.dispose()
			planeTex = undefined
		}
		if (planeGeo != null) {
			planeGeo.dispose()
			planeGeo = undefined
		}
	}

	planeGeo = createPlaneGeo()
	return createPlaneMat().then(
		pm => {planeMat = pm}
	).then(() => {
		return {
			planeGeo: planeGeo!,
			planeMat: planeMat!,
			dispose
		}
	})
}

export default Assets
