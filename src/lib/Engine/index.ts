import * as THREE from 'three'
import {capitalizeFirst} from '../string'
import Ptr from '../Ptr'
import V2 from '../vec/V2'
import Distortion from './Distortion'
import Assets from './Assets'

export interface Uniforms {
	distortions: Distortion[]
	toon: number
	saturation: number
	vignette: number
	warm: number
	cool: number
}

/** WebGL rendering engine */
interface Engine {
	readonly canvas: HTMLCanvasElement
	/** Caches last known canvas size in pixels. Initialized with default values. */
	readonly display: {
		readonly x: number
		readonly y: number
		readonly width: number
		readonly height: number
	}
	/**
	 * Resolves when all assets are loaded and scene is
	 * ready to render or rejects on error.
	 */
	readonly ready: Promise<void>
	/** When all assets are loaded and scene is ready to render */
	isReady(): boolean
	/** Indicates Engine animation loop is running */
	isRunning(): boolean
	/** Start animation loop (if not already running) */
	run(): void
	/** Pause animation loop */
	pause(): void
	/** Render a frame immediately */
	render(): void
	/** Set a single uniform value */
	setUniform (id: string, value: number): void
	/** Set shader application params */
	setUniforms(uniforms: Uniforms): void
	/** Set amount of one distortion by index */
	setDistortionAmount(index: number, amount: number): void
	/** Create a PNG data url from the canvas */
	createPngDataUrl(): string
	/** Create a PNG blob snapshot of the canvas */
	createPngBlob(size?: number): Promise<Blob>
	/** Invoke a resize. Engine will listen for resize events, but this can be called in cases where an event may not be fired. */
	resize(): void
	/** Dispose rendering context, free up resources */
	dispose(): void
}

function Engine (canvas: HTMLCanvasElement, distortions: Distortion[], dstEls: HTMLElement[], photo?: HTMLCanvasElement): Engine {
	if (distortions.length !== dstEls.length) {
		throw new Error('Number of distortions does not match number of distortion selection elements')
	}
	// Create a local working copy that's safe to mutate
	distortions = distortions.map(d => ({...d}))
	let assets: Assets | undefined
	/** Index of currently selected distortion */
	let selectedIndex: number | undefined
	const context = canvas.getContext('webgl2', {preserveDrawingBuffer: true}) as WebGLRenderingContext
	if (context == null) {
		throw new Error('WebGL2 rendering context not available')
	}
	const renderer = new THREE.WebGLRenderer({
		canvas, context, alpha: true
	})
	if (!renderer) {
		throw new Error("Failed to create THREE.WebGLRenderer")
	}
	/** Current display rect (updated on resize) */
	const display = {x: 0, y: 0, width: 512, height: 512}
	let planeMesh: THREE.Mesh | undefined
	let isReady = false
	let isRunning = false
	renderer.setClearColor(0, 1)
	renderer.setClearAlpha(1)
	const scene = new THREE.Scene()
	const camera = new THREE.OrthographicCamera(-1, 1, 1, -1, 0, 1)
	scene.add(camera)
	window.addEventListener('resize', resize)
	resize()

	const ptr = Ptr(canvas, {
		down: e => {
			// Try to select a distortion
			const p = V2.create(
				(e.x - display.x) / display.width,
				(display.height - (e.y - display.y)) / display.height
			)
			selectedIndex = Distortion.nearestWithin(p, distortions)
		},
		move: e => {
			// Move selected distortion (if any)
			if (selectedIndex == null) return
			const x = (e.x - display.x) / display.width
			const y = (display.height - (e.y - display.y)) / display.height
			setDistortionPosition(selectedIndex, x, y)
			render()
		},
		up: e => {
			// Deselect any selected distortion
			selectedIndex = undefined
		}
	})

	const ready = Assets(distortions, photo).then(a => {
		assets = a
		planeMesh = new THREE.Mesh(assets.planeGeo, assets.planeMat)
		scene.add(planeMesh)
		resize()
		isReady = true
	})

	function setUniforms (uniforms: Uniforms) {
		const ds = uniforms.distortions
		if (ds.length !== distortions.length) {
			throw new Error('Number of Distortions must be ' + distortions.length)
		}
		if (!planeMesh) {
			throw new Error('Mesh not ready yet')
		}
		const mat = planeMesh.material as THREE.RawShaderMaterial
		//mat.uniforms.uSpots.value = ds.map(d => new THREE.Vector4(d.x, d.y, d.r, 0))
		mat.uniforms.uToon.value = uniforms.toon
		mat.uniforms.uSaturation.value = uniforms.saturation
		mat.uniforms.uVignette.value = uniforms.vignette
		mat.uniforms.uWarm.value = uniforms.warm
		mat.uniforms.uCool.value = uniforms.cool
	}

	function setUniform (id: string, value: number) {
		if (!planeMesh) {
			throw new Error('Mesh not ready yet')
		}
		const mat = planeMesh.material as THREE.RawShaderMaterial
		const uname = 'u' + capitalizeFirst(id)
		mat.uniforms[uname].value = value
	}

	// function setDistortion (i: number, d: Distortion) {
	// 	if (!planeMesh) {
	// 		throw new Error('Mesh not ready yet')
	// 	}
	// 	const mat = planeMesh.material as THREE.RawShaderMaterial
	// 	distortions[i] = {...d}
	// 	mat.uniforms.uSpots.value[i] = new THREE.Vector4(
	// 		d.x, d.y, d.r, d.a
	// 	)
	// }

	function setDistortionPosition (i: number, x: number, y: number) {
		if (!planeMesh) {
			throw new Error('Mesh not ready yet')
		}
		const d = distortions[i]
		d.x = x
		d.y = y
		const mat = planeMesh.material as THREE.RawShaderMaterial
		mat.uniforms.uSpots.value[i].x = x
		mat.uniforms.uSpots.value[i].y = y
	}

	function setDistortionAmount (i: number, a: number) {
		if (!planeMesh) {
			throw new Error('Mesh not ready yet')
		}
		const mat = planeMesh.material as THREE.RawShaderMaterial
		const d = distortions[i]
		d.a = a
		mat.uniforms.uSpots.value[i] = new THREE.Vector4(
			d.x, d.y, d.r, d.a
		)
		dstEls[i].style.display = a > 0 ? 'block' : 'none'
	}

	/** Render this distortion's selection marquee */
	function renderDistSel (i: number) {
		const dst = distortions[i]
		const el = dstEls[i]
		const size = display.width * dst.r * 2
		const halfSize = size / 2
		el.style.width = `${size}px`
		el.style.height = `${size}px`
		el.style.left = `${dst.x * display.width - halfSize}px`
		el.style.top = `${display.height - dst.y * display.height - halfSize}px`
	}

	function createPngBlob (size?: number): Promise<Blob> {
		if (size != null) {
			if (!Number.isSafeInteger(size) || size < 1) {
				return Promise.reject(new Error('Invalid size for png blob: ' + size))
			}
		}
		let pngCanvas = canvas
		// If a specific size was requested we need to create a new canvas,
		// scale the image then use the scaled source.
		if (size != null && size !== canvas.width) {
			pngCanvas = document.createElement('canvas')
			pngCanvas.width = size
			pngCanvas.height = size
			const ctx = pngCanvas.getContext('2d')!
			ctx.drawImage(canvas, 0, 0, canvas.width, canvas.height, 0, 0, size, size)
		}
		return new Promise(resolve => {
			pngCanvas.toBlob(blob => resolve(blob!), 'image/png')
		})
	}

	function createPngDataUrl(): string {
		return canvas.toDataURL('image/png')
	}

	function render() {
		renderer.render(scene, camera)
		for (let i = 0; i < distortions.length; ++i) {
			renderDistSel(i)
		}
	}

	function renderLoop() {
		if (!isRunning) return
		render()
		requestAnimationFrame(renderLoop)
	}

	function resize() {
		const rc = canvas.getBoundingClientRect()
		display.x = rc.left
		display.y = rc.top
		canvas.width = display.width = rc.width
		canvas.height = display.height = rc.height
		// Tell three.js *not* to set canvas inline style width & height by passing false
		renderer.setSize(display.width, display.height, false)
		camera.updateProjectionMatrix()
		render()
	}

	function run() {
		if (!isRunning) {
			requestAnimationFrame(renderLoop)
			isRunning = true
		}
	}

	function pause() {
		isRunning = false
	}

	function dispose() {
		console.log('Disposing resources...')
		ptr.destroy()
		window.removeEventListener('resize', resize)
		if (assets != null) {
			assets.dispose()
			assets = undefined
		}
		scene.dispose()
		renderer.dispose()
		// Attempt to forcibly destroy this WebGL context
		const ext = context.getExtension('WEBGL_lose_context')
		if (ext != null) {
			ext.loseContext()
		}
		console.log('...done')
	}

	return {
		canvas, display, ready,
		isRunning: () => isRunning,
		isReady: () => isReady,
		setUniform, setUniforms, setDistortionAmount,
		createPngDataUrl, createPngBlob,
		run, pause,
		render, resize, dispose
	}
}

export default Engine
