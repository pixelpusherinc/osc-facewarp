/**
 * Wrapper API for WebSocket
 */
interface Socket {
	/**
	 * Promise that resolves when this socket is closed by the server
	 * or due to other external network conditons.
	 * Does not resolve when the socket is closed intentionally
	 * on the client side.
	 */
	readonly closed: Promise<void>
	/** Reference to underlying WebSocket */
	readonly ws: WebSocket
	/** Start listening for all messages with this id */
	on(id: string, f: Socket.HandlerFn): void
	/** Stop listening for messages with this id */
	off(id: string, f: Socket.HandlerFn): void
	/** Listen for just one message with this id */
	once(id: string, f: Socket.HandlerFn): void
	/**
	 * Promise that resolves with data for this message id.
	 * Can optionally be rejected on some timeout.
	 */
	one(id: string, timeout?: number): Promise<string>
	/** Send message to server */
	send(id: string, data?: string): void
	/** Close this socket */
	close(): void
}

namespace Socket {
	export type HandlerFn = (data: string | undefined) => void

	export function serialize (id: string, data?: string) {
		return data != null ? id + ' ' + data : id
	}

	export function deserialize (raw: string) {
		const i = raw.indexOf(' ')
		let id: string
		let data: string | undefined
		if (i >= 0) {
			id = raw.substr(0, i)
			data = raw.substr(i + 1)
		} else {
			id = raw
		}
		return {id, data}
	}

		/** Create a new Socket wrapper for the given URL */
	export function create (url: string): Promise<Socket> {
		const handlersMap: Record<string, Socket.HandlerFn[]> = Object.create(null)
		const ws = new WebSocket(url)

		/** Add the message listener */
		ws.addEventListener('message', evt => {
			const raw = String(evt.data)
			const i = raw.indexOf(' ')
			let id: string
			let data: string | undefined
			if (i >= 0) {
				id = raw.substr(0, i)
				data = raw.substr(i + 1)
			} else {
				id = raw
			}
			const handlers = handlersMap[id]
			if (handlers != null) {
				handlers.forEach(f => {f(data)})
			}
		})

		/** Start listening for all messages with this id */
		function on (id: string, f: Socket.HandlerFn) {
			if (handlersMap[id]) {
				handlersMap[id].push(f)
			} else {
				handlersMap[id] = [f]
			}
		}

		/** Stop listening for messages with this id */
		function off (id: string, f: Socket.HandlerFn) {
			const handlers = handlersMap[id]
			if (handlers == null) {
				return
			}
			const i = handlers.indexOf(f)
			if (i < 0) {
				return
			}
			handlers.splice(i, 1)
			if (handlers.length <= 0) {
				delete handlersMap[id]
			}
		}

		/** Listen for just one message with this id */
		function once (id: string, f: Socket.HandlerFn) {
			on(id, function handler(data) {
				off(id, handler)
				f(data)
			})
		}

		/**
		 * Promise that resolves with data for this message id.
		 * Can optionally be rejected on some timeout.
		 */
		function one (id: string, timeout?: number) {
			if (timeout == null) {
				return new Promise<string>(res => once(id, res))
			}
			return new Promise<string>((res, rej) => {
				let timer: number | undefined = window.setTimeout(() => {
					timer = undefined
					rej(new Error(`socket.one timed out waiting for message id: '${id}'`))
				}, timeout)
				once(id, data => {
					if (timer != null) {
						window.clearTimeout(timer)
						timer = undefined
						res(data)
					}
				})
			})
		}

		function send (message: string, data?: string) {
			ws.send(Socket.serialize(message, data))
		}

		const {closed, close} = (function() {
			/** Promise resolver for socket close */
			let closeResolver: () => void
			const onClose = () => closeResolver()
			return {
				closed: new Promise<void>(res => {
					closeResolver = res
					ws.addEventListener('close', onClose)
				}),
				close: () => {
					// Remove this event listener since it should only fire when
					// the server closes, not when we intentionally close.
					ws.removeEventListener('close', onClose)
					ws.close()
				}
			}
		}())

		return new Promise<Socket>((res, rej) => {
			let resolved = false
			ws.addEventListener('open', e => {
				resolved = true
				res({closed, ws, on, off, once, one, send, close})
			})
			ws.addEventListener('error', e => {
				console.error('Socket error:', e)
				if (!resolved) {
					resolved = true
					rej(new Error('Socket connection failed'))
				}
			})
		})
	}
}

export default Socket
