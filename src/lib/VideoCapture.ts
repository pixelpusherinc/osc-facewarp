interface VideoCapture {
	ready: Promise<MediaDeviceInfo[]>
	devices: MediaDeviceInfo[]
	/** Start video stream. Returns MediaStream when video started. */
	start(deviceId: string): Promise<MediaStream>
	/** Stop video stream */
	stop(): void
}

function VideoCapture ({width, height, bitrate}: VideoCapture.Options): VideoCapture {
	let devices: MediaDeviceInfo[] = []
	//let capture: {stream: MediaStream, url: string} | undefined
	let mediaStream: MediaStream | undefined

	const ready = navigator.mediaDevices.enumerateDevices().then((allDevices: MediaDeviceInfo[]) => {
		devices = allDevices.filter(d => d.kind === 'videoinput')
		if (devices.length > 0) {
			console.log("Found videoinput devices:", devices)
		} else {
			console.warn("No video devices found!")
		}
		return devices
	})

	/** Start video stream */
	function start (deviceId: string) {
		if (mediaStream != null) {
			console.log("Already started")
			return Promise.resolve(mediaStream)
		}
		if (!devices.some(d => d.deviceId === deviceId)) {
			return Promise.reject(new Error('Unrecognized device ID: ' + deviceId))
		}

		console.log(`Requesting video stream: ${width}x${height}`)

		// Request camera video
		return navigator.mediaDevices.getUserMedia({
			audio: false,
			video: {deviceId, width, height}
		}).then((mstr: MediaStream) => {
			console.log("Got camera video stream:", mstr)
			mediaStream = mstr
			return mediaStream
		})
	}

	/** Stop video stream */
	function stop() {
		if (mediaStream) {
			for (const trk of mediaStream.getTracks()) {
				trk.stop()
			}
			mediaStream = undefined
		} else {
			console.log("Not started")
		}
	}

	return {
		ready, devices, start, stop
	}
}

namespace VideoCapture {
	export interface Options {
		width: number
		height: number
		bitrate: number
	}
}

export default VideoCapture
