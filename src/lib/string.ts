// string utils

/**
 * @param t Time in ms
 * @returns Formatted string
 */
export function formatTime (t: number) {
	const ts = Math.round(t / 1000)
	const m = Math.floor(ts / 60)
	const ss = ts % 60
	const s = (ss >= 10 ? '' : '0') + ss.toString()
	return `${m}:${s}`
}

/** Truncate string to length, ending with ellipsis character if shortened */
export function ellipsis (str: string, length: number) {
	if (str.length < length) return str
	if (length < 2) return str.substr(0, Math.max(0, length))
	return str.substr(0, length - 1).trimEnd() + '…'
}

export function capitalizeFirst (str: string) {
	if (typeof str !== 'string') {
		throw new Error('string required')
	}
	return str ? str[0].toUpperCase() + str.slice(1) : ''
}
