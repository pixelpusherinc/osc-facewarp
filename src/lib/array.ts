/** Generate an array sequence of num numbers starting from 0 incrementing by 1 */
export function range (num: number): number[]
/** Generate an array sequence of numbers starting from start up to but not including end, incrementing by 1 */
export function range (start: number, end: number): number[]
/** Generate an array sequence of numbers from start up to but not including end incrementing by step */
export function range (start: number, end: number, step: number): number[]

export function range (start: number, end?: number, step?: number): number[] {
	step = step || 1
	if (end == null) {
		end = start
		start = 0
	}
	const size = Math.ceil((end - start) / step)
	const a: number[] = []
	for (let i = 0; i < size; ++i) {
		a.push(start + step * i)
	}
	return a
}

export function sample<T>(arr: Array<T>, size?: number): Array<T> {
	if (size == null) size = arr.length
	if (!size || size < 1 || arr.length < 1) return []
	if (size > arr.length) size = arr.length
	const result: T[] = []
	const a = arr.slice()
	for (let i = 0; i < size; ++i) {
		const r = Math.floor(Math.random() * a.length)
		result.push(a[r])
		a.splice(r, 1)
	}
	return result
}

/** Helper for arrays since this built-in doesn't exist */
export function findLastIndex(predicate: (v: number, i?: number) => boolean, arr: ArrayLike<number>) {
	for (let i = arr.length - 1; i >= 0; --i) {
		if (predicate(arr[i], i)) {
			return i
		}
	}
	return -1
}
