// Math utils

export const PI2: number = Math.PI * 2

export function roundTo (n: number, places: number) {
	const d = Math.pow(10, places)
	return Math.round(n * d) / d
}

export function clamp (n: number, min: number, max: number) {
	return Math.min(Math.max(n, min), max)
}

export function smoothstep(edge0: number, edge1: number, x: number) {
	// Scale, bias and saturate x to 0..1 range
	x = clamp((x - edge0) / (edge1 - edge0), 0.0, 1.0)
	// Evaluate polynomial
	return x * x * (3 - 2 * x)
}

export function smootherstep(edge0: number, edge1: number, x: number) {
	// Scale, and clamp x to 0..1 range
	x = clamp((x - edge0) / (edge1 - edge0), 0.0, 1.0)
	// Evaluate polynomial
	return x * x * x * (x * (x * 6 - 15) + 10)
}

/** Always positive modulus */
export function pmod (n: number, m: number) {
	return (n % m + m) % m
}

/** A random number from -1.0 to 1.0 */
export function nrand() {
	return Math.random() * 2 - 1
}

export function angle (x: number, y: number) {
	return pmod(Math.atan2(y, x), PI2)
}

export function difAngle (a0: number, a1: number) {
	const r = pmod(a1, PI2) - pmod(a0, PI2)
	return Math.abs(r) < Math.PI ? r : r - PI2 * Math.sign(r)
}

export function dot (x0: number, y0: number, x1: number, y1: number): number {
	return x0 * x1 + y0 * y1
}

/**
 * Linear interplation from x to y.
 * a must be from 0.0 to 1.0
 */
export function lerp (x: number, y: number, a: number): number {
	const b = 1 - a
	return x * b + y * a
}

export const mix = lerp

export function lerpAngle (x: number, y: number, a: number): number {
	const d = difAngle(x, y)
	return pmod(x + d * a, PI2)
}

/**
 * Trigonometric interpolation from x to y (smoothed at endpoints.)
 * a must be from 0.0 to 1.0
 */
export function terp (x: number, y: number, a: number): number {
	const r = Math.PI * a
	const s = (1 - Math.cos(r)) * 0.5
	const t = 1 - s
	return x * t + y * s
}

/**
 * Exponential interpolation
 * @param x Start value
 * @param y End value
 * @param a Amount (0-1)
 * @param e Exponent
 */
export function xerp (x: number, y: number, a: number, e: number): number {
	let s: number
	if (a < 0.5) {
		s = Math.pow(a * 2, e) / 2
	} else {
		s = 1 - (a - 0.5) * 2
		s = Math.pow(s, e) / 2
		s = 1 - s
	}
	return lerp (x, y, s)
}
