const DEVICE_NONE  = 0
const DEVICE_MOUSE = 1
const DEVICE_TOUCH = 2

export type PtrEventType = 'down' | 'move' | 'up'

export class PtrEvent {
	type: PtrEventType
	x: number
	y: number
	domEvent: MouseEvent | TouchEvent
	constructor (type: PtrEventType, x: number, y: number, e: MouseEvent | TouchEvent) {
		this.type = type
		this.x = x
		this.y = y
		this.domEvent = e
	}
}

export interface PtrEventListeners {
	down?(e: PtrEvent): void
	up?(e: PtrEvent): void
	move?(e: PtrEvent): void
}

/** Mouse/Touch input abstraction for down/up/move events */
interface Ptr {
	destroy(): void
}

/**
 * Creates a new Ptr instance attached to the element
 */
function Ptr (el: HTMLElement, listeners: PtrEventListeners): Ptr {
	let device = DEVICE_NONE

	function onMouseDown (e: MouseEvent) {
		e.preventDefault()
		if (device === DEVICE_TOUCH) {
			return
		}
		device = DEVICE_MOUSE
		window.addEventListener('mousemove', onMouseMove)
		window.addEventListener('mouseup', onMouseUp)
		if (listeners.down) {
			listeners.down(new PtrEvent('down', e.pageX, e.pageY, e))
		}
	}

	function onTouchStart (e: TouchEvent) {
		e.preventDefault()
		if (device === DEVICE_MOUSE) {
			return
		}
		device = DEVICE_TOUCH
		el.addEventListener('touchmove', onTouchMove)
		el.addEventListener('touchend', onTouchEnd)
		if (listeners.down) {
			const t = e.changedTouches[0]
			listeners.down(new PtrEvent('down', t.pageX, t.pageY, e))
		}
	}

	function onMouseMove (e: MouseEvent) {
		e.preventDefault()
		if (listeners.move) {
			listeners.move(new PtrEvent('move', e.pageX, e.pageY, e))
		}
	}

	function onTouchMove (e: TouchEvent) {
		e.preventDefault()
		if (listeners.move) {
			const t = e.changedTouches[0]
			listeners.move(new PtrEvent('move', t.pageX, t.pageY, e))
		}
	}

	function onPointerUp (x: number, y: number, e: MouseEvent | TouchEvent) {
		e.preventDefault()
		const d = device
		setTimeout(() => {
			if (device === d) {
				device = DEVICE_NONE
			}
		}, 200)
		if (listeners.up) {
			listeners.up(new PtrEvent('up', x, y, e))
		}
	}

	function onMouseUp (e: MouseEvent) {
		window.removeEventListener('mouseup', onMouseUp)
		window.removeEventListener('mousemove', onMouseMove)
		onPointerUp(e.pageX, e.pageY, e)
	}

	function onTouchEnd (e: TouchEvent) {
		el.removeEventListener('touchend', onTouchEnd)
		el.removeEventListener('touchmove', onTouchMove)
		const t = e.changedTouches[0]
		onPointerUp(t.pageX, t.pageY, e)
	}

	function destroy() {
		window.removeEventListener('mousemove', onMouseMove)
		window.removeEventListener('mouseup', onMouseUp)
		el.removeEventListener('touchmove', onTouchMove)
		el.removeEventListener('touchend', onTouchEnd)
		el.removeEventListener('mousedown', onMouseDown)
		el.removeEventListener('touchstart', onTouchStart)
	}

	// Initialize by only adding the down/start listeners.
	// We add move/up listeners later on press.
	el.addEventListener('mousedown', onMouseDown)
	el.addEventListener('touchstart', onTouchStart)

	return {destroy}
}

export default Ptr
