export type PersonalityTrait = 'O' | 'C' | 'E' | 'A' | 'N'

export type TraitSign = 1 | -1

export interface OceanScores {
	O: number
	C: number
	E: number
	A: number
	N: number
}

export interface OceanResults {
	en: Record<PersonalityTrait, string[]>
	fr: Record<PersonalityTrait, string[]>
}

export const RESULTS: OceanResults = {
	en: {
		O: ['Routine', 'Conventional', 'Practical', 'Independent', 'Curious'],
		C: ['Impulsive', 'Disorganized', 'Thoughtful', 'Organized', 'Hardworking'],
		E: ['Reserved', 'Quiet', 'Social', 'Outgoing', 'Adventurous'],
		A: ['Critical', 'Guarded', 'Agreeable', 'Cooperative', 'Helpful'],
		N: ['Secure', 'Calm', 'Concerned', 'Anxious', 'Easily upset']
	},
	fr: {
		O: ['routinière', 'conventionnelle', 'pratique', 'indépendante', 'curieuse'],
		C: ['impulsive', 'désorganisée', 'prévenante', 'organisée', 'travailleuse'],
		E: ['réservée', 'silencieuse', 'sociable', 'enjouée', 'aventureuse'],
		A: ['critique', 'méfiante', 'accommodante', 'conciliante', 'serviable'],
		N: ['paisible', 'calme', 'inquiète', 'anxieuse', 'irritable']
	}
}

export const RESULT_RANGES = [1.5, 2.5, 4, 5, 1000]

/** Converts a score to a result index */
export function scoreToIndex (score: number) {
	for (let i = 0; i < RESULT_RANGES.length; ++i) {
		if (score < RESULT_RANGES[i]) {
			return i
		}
	}
	console.warn(`Score out of range! (${score})`)
	return RESULT_RANGES.length - 1
}

export function scoreToString (scores: OceanScores, trait: PersonalityTrait, lang: 'en' | 'fr'): string {
	const rtext = RESULTS[lang]
	const score = scores[trait]
	return rtext[trait][scoreToIndex(score)]
}
