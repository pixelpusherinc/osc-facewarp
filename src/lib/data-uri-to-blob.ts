/** Convert base64/URLEncoded data component to raw binary data held in a string */
export default function dataURItoBlob (dataURI: string) {
	let byteString: string
	const parts = dataURI.split(',')
	if (parts[0].indexOf('base64') >= 0) {
		byteString = atob(parts[1])
	} else {
		byteString = decodeURIComponent(parts[1])
	}

	// separate out the mime component
	const mimeString = parts[0].split(':')[1].split(';')[0]

	// write the bytes of the string to a typed array
	const ia = new Uint8Array(byteString.length)
	for (let i = 0; i < byteString.length; i++) {
		ia[i] = byteString.charCodeAt(i)
	}
	return new Blob([ia], {type: mimeString})
}
