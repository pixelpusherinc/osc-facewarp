import {equalish as eq} from './common'

interface V2 {
	x: number
	y: number
}

class V2 implements V2 {
	protected constructor (x?: number, y?: number) {
		this.x = typeof x === 'number' ? x : 0
		this.y = typeof y === 'number' ? y : 0
	}

	static create (x?: number, y?: number): V2 {
		return new V2(x, y)
	}

	static of (v: Partial<V2>): V2 {
		return new V2(v.x, v.y)
	}

	static set (out: V2, x?: number, y?: number): V2 {
		out.x = typeof x === 'number' ? x : out.x
		out.y = typeof y === 'number' ? y : out.y
		return out
	}

	static copy (out: V2, v: V2): V2 {
		out.x = v.x
		out.y = v.y
		return out
	}

	static equalish (a: V2, b: V2): boolean {
		return eq(a.x, b.x) && eq(a.y, b.y)
	}

	static toArray<T extends {[n: number]: number}> (out: T, v: V2, offset = 0): T {
		out[offset + 0] = v.x
		out[offset + 1] = v.y
		return out
	}

	static fromArray (out: V2, a: ArrayLike<number>, offset = 0): V2 {
		out.x = a[offset + 0]
		out.y = a[offset + 1]
		return out
	}

	static add (out: V2, a: V2, b: V2): V2 {
		out.x = a.x + b.x
		out.y = a.y + b.y
		return out
	}

	static sub (out: V2, a: V2, b: V2): V2 {
		out.x = a.x - b.x
		out.y = a.y - b.y
		return out
	}

	static scale (out: V2, v: V2, s: number): V2 {
		out.x = v.x * s
		out.y = v.y * s
		return out
	}

	static len (v: V2): number {
		return Math.sqrt(v.x * v.x + v.y * v.y)
	}

	static lenSq (v: V2): number {
		return v.x * v.x + v.y * v.y
	}

	static setLen (out: V2, v: V2, l: number): V2 {
		let s = V2.len(v)
		if (s > 0) {
			s = l / s
			out.x = v.x * s
			out.y = v.y * s
		} else {
			out.x = l
			out.y = 0
		}
		return out
	}

	static normalize (out: V2, v: V2): V2 {
		return V2.setLen(out, v, 1)
	}

	static dist (a: V2, b: V2): number {
		const dx = b.x - a.x
		const dy = b.y - a.y
		return Math.sqrt(dx * dx + dy * dy)
	}

	static distSq (a: V2, b: V2): number {
		const dx = b.x - a.x
		const dy = b.y - a.y
		return dx * dx + dy * dy
	}

	static dot (a: V2, b: V2): number {
		return a.x * b.x + a.y * b.y
	}

	static angle (v: V2): number {
		return Math.atan2(v.y, v.x)
	}

	static rotate (out: V2, v: V2, r: number): V2 {
		const c = Math.cos(r),
			s = Math.sin(r),
			x = v.x, y = v.y
		out.x = x * c - y * s
		out.y = x * s + y * c
		return out
	}

	/** nx,ny should be normalized; vx,vy length will be preserved */
	static reflect (out: V2, v: V2, n: V2): V2 {
		const d = V2.dot(n, v)
		out.x = v.x - 2 * d * n.x
		out.y = v.y - 2 * d * n.y
		return out
	}

	/** Multiply V2 by a 2x2 matrix (4 elements) */
	static multM2 (out: V2, v: V2, m: ArrayLike<number>): V2 {
		const x = v.x, y = v.y
		out.x = m[0] * x + m[2] * y
		out.y = m[1] * x + m[3] * y
		return out
	}

	/** Multiply V2 by a 3x3 matrix (9 elements) */
	static multM3 (out: V2, v: V2, m: ArrayLike<number>): V2 {
		const x = v.x, y = v.y
		out.x = m[0] * x + m[3] * y + m[6]
		out.y = m[1] * x + m[4] * y + m[7]
		return out
	}

	/** Format vector as a string having `p` decimal places. */
	static format (v: V2, p = 1) {
		return `{${v.x.toFixed(p)}, ${v.y.toFixed(p)}}`
	}
}

export default V2
