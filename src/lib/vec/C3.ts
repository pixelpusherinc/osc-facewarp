import {equalish as eq} from './common'

interface C3 {
	r: number
	g: number
	b: number
}

class C3 implements C3 {
	protected constructor (r?: number, g?: number, b?: number) {
		this.r = typeof r === 'number' ? r : 0
		this.g = typeof g === 'number' ? g : 0
		this.b = typeof b === 'number' ? b : 0
	}

	static create (r?: number, g?: number, b?: number): C3 {
		return new C3(r, g, b)
	}

	static of (c: Partial<C3>) {
		return new C3(c.r, c.g, c.b)
	}

	static set (out: C3, r?: number, g?: number, b?: number) {
		out.r = typeof r === 'number' ? r : out.r
		out.g = typeof g === 'number' ? g : out.g
		out.b = typeof b === 'number' ? b : out.b
		return out
	}

	static copy (dst: C3, src: C3) {
		dst.r = src.r
		dst.g = src.g
		dst.b = src.b
		return dst
	}

	static equalish (a: C3, b: C3): boolean {
		return eq(a.r, b.r) && eq(a.g, b.g) && eq(a.b, b.b)
	}

	static toArray<T extends {[n: number]: number}> (out: T, c: C3, offset = 0) {
		out[offset + 0] = c.r
		out[offset + 1] = c.g
		out[offset + 2] = c.b
		return out
	}

	static fromArray (out: C3, a: ArrayLike<number>, offset = 0) {
		out.r = a[offset + 0]
		out.g = a[offset + 1]
		out.b = a[offset + 2]
		return out
	}

	static toHex (c: C3) {
		return (c.r * 255) << 16 ^ (c.g * 255) << 8 ^ (c.b * 255) << 0
	}

	static fromHex (out: C3, hex: number) {
		const h = Math.floor(hex)
		out.r = (h >> 16 & 255) / 255
		out.g = (h >> 8 & 255) / 255
		out.b = (h & 255) / 255
		return out
	}

	static toHexString (c: C3) {
		return ('000000' + C3.toHex(c).toString(16)).slice(-6)
	}
}

export default C3
