interface FPS {
	update(dt: number): number
	fps(): number
}

/**
 * Create instance of Frames Per Second monitor
 */
function FPS (num = 16): FPS {
	const ticks = new Array<number>(num)
	let index = 0
	let f = 60.0  // frames per sec initial assumption
	for (let i = 0; i < num; ++i) {
		ticks[i] = 1000 / f
	}
	let sum = num * 1000 / f

	/**
	 *  Update with new sample
	 *  @returns New average frames/second
	 */
	function update (dt: number) {
		sum -= ticks[index]
		sum += dt
		ticks[index] = dt
		index = (index + 1) % num
		f = 1000 * num / sum
		return f
	}

	/** @returns current average frames/second */
	function fps() {
		return f
	}

	return {update, fps}
}

export default FPS
