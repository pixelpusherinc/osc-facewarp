import * as THREE from 'three'

export async function loadText (url: string): Promise<string> {
	const r = await fetch(url)
	return r.text()
}

export function loadImage (url: string): Promise<HTMLImageElement> {
	return new Promise((resolve, reject) => {
		const image = new Image()
		image.onload = () => {resolve(image)}
		image.onerror = e => {reject(e)}
		image.src = url
	})
}

export function loadTexture (url: string): Promise<THREE.Texture> {
	return new Promise((resolve, reject) => {
		const loader = new THREE.TextureLoader()
		loader.load(
			url,
			texture => {
				resolve(texture)
			},
			undefined,
			e => {
				reject(e.message ? e : new Error('Failed to load texture: ' + url))
			}
		)
	})
}

export function loadGeometry (url: string) {
	return new Promise<THREE.BufferGeometry>((resolve, reject) => {
		const loader = new THREE.BufferGeometryLoader()
		loader.load(url, resolve, undefined,
			(xhr: XMLHttpRequest) => {
				reject(new Error(`Failed to load '${url}' (${xhr.status})`))
			}
		)
	})
}
