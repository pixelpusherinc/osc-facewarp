interface TimeSlice {
	/** Start time of time slice (ms) */
	t: number
	/** Duration (delta) of time slice (ms) */
	d: number
}

function TimeSlice(t: number, d: number): TimeSlice {
	return {t, d}
}

export default TimeSlice
