import m from 'mithril'
import Stream from 'mithril/stream'
import Observer from '../Observer'

export interface Attrs {
	/** Progress value from 0..1 */
	progress$: Stream<number>
}

/** Loading Bar component */
const LoadingBar: m.Component<Attrs> = {
	// Uses the observer component to handle stream management
	// and granular vdom re-rendering.
	view: ({attrs: {progress$}}) => m(Observer, {
		data: progress$,
		render: (p: number) => m('.bar', {
			style: `width: ${p * 100}%`
		}),
		view: () => m('.loading-bar')
	})
}

export default LoadingBar
