// WebGL util funcs

let supported: boolean | undefined

/** Returns true if WebGL support detected */
export function detectWebGL() {
	if (supported == null) {
		try {
			const canvas = document.createElement('canvas')
			supported = !!canvas.getContext('webgl')
				|| !!canvas.getContext('experimental-webgl')
				|| !!canvas.getContext('webgl2')
		} catch (e) {
			supported = false
		}
	}
	return supported
}
