# Face Distortion Experiment

## Install

	npm install

### Recommended VS Code extensions:

* TSLint
* stylelint
* postcss-sugarss-language
* EditorConfig

## Compile-on-save ts and css files with source maps, start localhost server

Use this during development:

	npm start

For the touch-input app: `http://localhost:3000/`

For the viewer app: `http://localhost:3000/viewer.html`

Edits to source files will be automatically compiled on save and reloaded in the browser.

## Compile minified JS and CSS

Use this to create optimized bundle files in `/public`:

	npm run build

## Notes

### Toon Shader

https://www.geeks3d.com/20140523/glsl-shader-library-toonify-post-processing-filter/

http://coding-experiments.blogspot.com/2011/01/toon-pixel-shader.html

### WebGL to WebGL2 migration

https://webgl2fundamentals.org/webgl/lessons/webgl1-to-webgl2.html
