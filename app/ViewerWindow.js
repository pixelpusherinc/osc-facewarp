//@ts-check
'use strict'

const fs = require('fs')
const path = require('path')
const Electron = require('electron')

/**
 * Creates the Electron Browser Window used for the viewer screen.
 * @param {{
 *  url: Promise<string>;
 *  bgcolor: string;
 *  fullscreen?: boolean;
 *  devtools?: boolean
 * }} opts
 */
module.exports = function ViewerWindow(opts) {
	const w = new Electron.BrowserWindow({
		title: 'Viewer',
		backgroundColor: opts.bgcolor,
		...(opts.fullscreen ? {
			x: config.x, y: config.y,
			fullscreen: true
		} : {
			width: 680 + (opts.devtools ? 320 : 0),
			height: 920
		}),
		webPreferences: {
			contextIsolation: true,
			nodeIntegration: false
			//preload: path.join(__dirname, 'preload.js')
		}
	})

	// Clear the webview cache
	w.webContents.session.clearCache(() => {
		console.log('Viewer webview cache cleared')
		opts.url.then(url => {w.loadURL(url)})
	})

	if (opts.devtools) {
		w.webContents.openDevTools()
	}

	return w
}

/** Config loaded from JSON file */
const config = (function() {
	const c = {x: 0, y: -1920} // Default
	try {
		const src = fs.readFileSync(path.join(__dirname, '..', 'viewer.config.json'), 'utf8')
		const data = JSON.parse(src)
		c.x = Number(data.x) || 0
		c.y = Number(data.y) || 0
	} catch (err) {
		console.log('Custom viewer.config.json not found, using defaults')
	}
	return c
}())
