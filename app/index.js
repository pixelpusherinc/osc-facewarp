//@ts-check
'use strict'

const run = require('./app')

const args = process.argv.slice(2)

const fullscreen = !args.includes('-w')
const devtools = args.includes('-d')

run({fullscreen, devtools}).catch(err => {
	console.error(err.stack)
	process.exit(1)
})
