//@ts-check
'use strict'

const path = require('path')

/** Application Configs */
module.exports = {
	HTTP_PORT: 3000,
	PUBLIC_WWW: path.resolve(__dirname, '..', 'public'),
	UPLOAD_HOME: path.resolve(__dirname, '..', 'public', 'upload'),
	PLACEHOLDERS_HOME: path.resolve(__dirname, '..', 'public', 'gallery-placeholders'),
	APP_TITLE: 'OSC FaceWarp',
	BG_COLOR: '#FFFFFF',
	/** Max age of upload file (in seconds) */
	MAX_UPLOAD_AGE: 11 * 60 * 60
}
