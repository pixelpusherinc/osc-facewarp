//@ts-check
'use strict'

// process.noAsar = true

const Electron = require('electron')
//Electron.app.commandLine.appendSwitch('ignore-certificate-errors', 'true')
Electron.app.commandLine.appendSwitch('allow-insecure-localhost', 'true')
const {startServer, stopServer} = require('./server')
const InterfaceWindow = require('./InterfaceWindow')
const ViewerWindow = require('./ViewerWindow')
const config = require('./config')

/*
 * Initialize Electron Inter-Process Communication.
 * Listens for messages from client
 * (Will we actually need to receive anything...?)
 *
function initIpc() {
	ipcMain.on('asynchronous-message', (event, arg) => {
		console.log('received message:', arg)
		const ipcClient = event.sender
		ipcClient.send('asynchronous-reply', 'pong')
	})
}*/

/**
 * Run the application
 * @param {{fullscreen?: boolean; devtools?: boolean}?} opts
 */
async function run(opts) {
	/**
	 * This will be a reference to the Interface window once it is created
	 * @type {Electron.BrowserWindow | null | undefined}
	 */
	let mainWindow

	/**
	 * This will be a reference to the Viewer window once it is created
	 * @type {Electron.BrowserWindow | null | undefined}
	 */
	let viewerWindow

	/** Creates the Interface window (if not yet created) */
	function createMainWindow() {
		if (mainWindow != null) return
		mainWindow = InterfaceWindow({
			bgcolor: config.BG_COLOR,
			...opts,
			url: urlsPromise.then(urls => urls[0])
		})
		mainWindow.on('closed', () => {mainWindow = null})
	}

	/** Creates the Viewer window (if not yet created) */
	function createViewerWindow() {
		if (viewerWindow != null) return
		viewerWindow = ViewerWindow({
			bgcolor: config.BG_COLOR,
			...opts,
			url: urlsPromise.then(urls => urls[1])
		})
		viewerWindow.on('closed', () => {viewerWindow = null})
	}

	/** Creates all application windows (if not yet created) */
	function createWindows() {
		createMainWindow()
		createViewerWindow()
	}

	/**
	 * Call this with a URL string once the Express server is up & running.
	 * @type {(urls: string[]) => void}
	 */
	let resolveUrls
	/** @type {Promise<string[]>} */
	const urlsPromise = new Promise(resolve => {
		resolveUrls = resolve
	})

	// Must hide window menus this way in Electron 6?+
	Electron.Menu.setApplicationMenu(null)

	// This method will be called when Electron has finished
	// initialization and is ready to create browser windows.
	// Some APIs can only be used after this event occurs.
	Electron.app.on('ready', () => {
		// Set up Content Security Policy
		// Electron.session.defaultSession.webRequest.onHeadersReceived(
		// 	(details, callback) => {
		// 		callback({
		// 			responseHeaders: {
		// 				...details.responseHeaders,
		// 				'Content-Security-Policy': [`default-src 'self'`]
		// 			}
		// 		})
		// 	}
		// )

		// Create Electron Browser Windows
		createWindows()
		// Init Electron IPC
		//initIpc()
	})

	// Quit when all windows are closed.
	Electron.app.on('window-all-closed', () => {
		// On OS X it is common for applications and their menu bar
		// to stay active until the user quits explicitly with Cmd + Q
		//if (process.platform !== 'darwin') {
		Electron.app.quit()
		//}
	})

	Electron.app.on('activate', () => {
		// On OS X it's common to re-create a window in the app when the
		// dock icon is clicked and there are no other windows open.
		createWindows()
	})

	Electron.app.on('quit', () => {
		console.log('Quitting')
		stopServer()
		mainWindow = undefined
		viewerWindow = undefined
	})

	try {
		await startServer()
		console.log('HTTP server started ok')
	} catch (err) {
		console.error(err.stack)
		process.exit(1)
	}

	// Resolve the promise to the URLs, which lets the windows load their web contents
	resolveUrls([
		`http://localhost:${config.HTTP_PORT}/${opts.fullscreen ? '?fullscreen=true' : ''}`,
		`http://localhost:${config.HTTP_PORT}/viewer.html${opts.fullscreen ? '?fullscreen=true' : ''}`
	])
}

module.exports = run
