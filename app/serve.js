//@ts-check
'use strict'

const {startServer} = require('./server')

startServer().then(url => {
	console.log(`HTTP server running on ${url}`)
}).catch(err => {
	console.error(err.stack)
	process.exit(1)
})
