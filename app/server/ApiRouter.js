//@ts-check
'use strict'

const path = require('path')
const fs = require('fs-extra')
const Express = require('express')
const ash = require('express-async-handler')
const asyncBusboy = require('async-busboy')
const {UPLOAD_HOME, PLACEHOLDERS_HOME, MAX_UPLOAD_AGE} = require('../config')
const {filterDirectory} = require('./lib')

/** Matches filenames with the pattern 1234567890.png (Unix timestamp format) */
const RX_FILENAME = /^[0-9]{10}\.png$/
const RX_TIMESTAMP = /^[0-9]{10}$/

/** Create the API Router */
module.exports = function ApiRouter() {
	const apiRouter = Express.Router()

	apiRouter.get('/test', (req, res) => {
		res.json({status: 'ok'})
	})

	/** Get a list of uploaded images */
	apiRouter.get('/list', ash(async (req, res) => {
		const filenames = (await listUploads()).slice(0, 20).map(
			f => `/upload/${f}`
		)
		res.json({files: filenames})
	}))

	/** Get a list of placeholder images */
	apiRouter.get('/placeholders', ash(async (req, res) => {
		const filenames = (await listPlaceholders()).slice(0, 20).map(
			f => `/gallery-placeholders/${f}`
		)
		res.json({files: filenames})
	}))

	/** Upload temp user photo */
	apiRouter.post('/temp-upload', ash(async (req, res) => {
		await upload(req, res, 'temp.png')
	}))

	/** Upload preview user image */
	apiRouter.post('/upload-preview', ash(async (req, res) => {
		await upload(req, res, `preview.png`)
	}))

	/** Upload finished user image */
	apiRouter.post('/upload', ash(async (req, res) => {
		await upload(req, res, `${Math.floor(Date.now() / 1000)}.png`)
	}))

	/** Delete a file specified by timestamp */
	apiRouter.post('/delete', ash(async (req, res) => {
		const t = req.body.timestamp
		if (!t || !RX_TIMESTAMP.test(t)) {
			res.status(400).send({message: 'Invalid file timestamp to delete'})
			return
		}
		await fs.unlink(path.join(UPLOAD_HOME, t + '.png'))
		res.send({status: 'ok'})
	}))

	/** Delete expired uploads */
	apiRouter.post('/clean-old', ash(async (req, res) => {
		const time = Math.floor(Date.now() / 1000)
		const files = await listUploads()
		for (const file of files) {
			const tstr = timestampFromUrl(file)
			const t = Number(tstr)
			if (time - t > MAX_UPLOAD_AGE) {
				console.log('Deleting expired upload: ' + file)
				await fs.unlink(path.join(UPLOAD_HOME, file))
			}
		}
		res.send({status: 'ok'})
	}))

	return apiRouter
}

const RX_IMGURL = /([0-9]{10})\.png$/

/** @param {string} url */
function timestampFromUrl (url) {
	const r = RX_IMGURL.exec(url)
	if (!r || !r[1]) {
		return undefined
	}
	return r[1]
}

async function listUploads() {
	const files = await filterDirectory(
		UPLOAD_HOME, (f, s) => s.isFile() && RX_FILENAME.test(f)
	)
	return files.sort((a, b) => a <= b ? 1 : -1)
}

async function listPlaceholders() {
	const files = await filterDirectory(
		PLACEHOLDERS_HOME, (f, s) => s.isFile() && f.endsWith('.png')
	)
	return files.sort((a, b) => a >= b ? 1 : -1)
}

/**
 * Handles an upload request, saves file using the given filename
 * @param {Express.Request} req
 * @param {Express.Response} res
 * @param {string} filename
 */
async function upload (req, res, filename) {
	const {files, fields} = await asyncBusboy(req)
	if (!files || files.length !== 1) {
		console.error(`Expected 1 file but got ${files ? files.length : 0}`)
		res.status(400).json({
			message: `Expected exactly 1 file upload but got ${files ? files.length : 0}`
		})
		return
	}
	const file = files[0]
	//const basename = `${Math.floor(Date.now() / 1000)}.png`
	console.log(`received fields: ${JSON.stringify(fields)} and ${files ? files.length : 0} files`)
	const dstFilename = path.join(UPLOAD_HOME, filename)
	const strm = file.pipe(fs.createWriteStream(dstFilename))
	strm.on('close', () => {
		console.log(`Saved file '${filename}'`)
		res.json({url: `/upload/${filename}`})
	})
	strm.on('error', err => {
		console.error(`Error writing file '${filename}':`, err)
		res.status(500).json({message: 'Error saving file'})
	})
}
