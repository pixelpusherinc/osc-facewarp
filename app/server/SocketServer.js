//@ts-check
'use strict'

const {Server} = require('http') // eslint-disable-line no-unused-vars
const WebSocket = require('ws')

/** @param {Server} server */
function SocketServer (server) {
	const wss = new WebSocket.Server({server})
	/** @type {WebSocket[]} */
	const sockets = []
	let clientCount = 0

	wss.on('connection', socket => {
		sockets.push(socket)
		clientCount += 1
		const clientId = clientCount
		console.log('Socket client ' + clientId + ' connected')

		socket.once('close', () => {
			console.log("Socket client " + clientId + " disconnected")
			const i = sockets.indexOf(socket)
			if (i >= 0) {
				sockets.splice(i, 1)
			} else {
				console.warn('Socket got close message for already closed socket')
			}
		})

		/** Handle messages from this socket */
		socket.on('message', raw => {
			if (!raw) {
				console.warn('Got socket message with empty data')
				return
			}
			// Relay to other clients
			relayMessage(raw, socket)
			// const {id, data} = deserialize(raw)
		})
	})

	/**
	 * Relay message to all clients except the specified one.
	 * @param {any} raw
	 */
	function relayMessage (raw, socket) {
		for (const s of sockets) {
			if (s !== socket) {
				s.send(raw, onSend)
			}
		}
	}

	/** Close any open sockets in this room */
	function close() {
		for (const s of sockets) {
			s.close()
			console.log('closed socket')
		}
		sockets.length = 0
	}

	/** @param {Error?} err */
	function onSend (err) {
		err && console.warn("socket.send error:", err.message)
	}

	/**
	 * Send message to all connected clients.
	 * @param {string} id
	 * @param {string?} data
	 * @returns {boolean}
	 */
	function send (id, data) {
		if (sockets.length < 1) {
			console.warn("Can't send: no open sockets")
			return false
		}

		const msg = serialize(id, data)

		sockets.forEach(socket => {
			socket.send(msg, onSend)
		})
		return true
	}

	return {send, close}
}

/**
 * Seralize message and data into one string
 * @param {string} id
 * @param {string?} data
 */
function serialize (id, data) {
	if (typeof id !== 'string') {
		throw new Error('Invalid id type')
	}
	if (id.length < 1) {
		throw new Error('Invalid id (empty string)')
	}
	return data ? id + ' ' + data : id
}

/** @param {string} raw */
function deserialize (raw) {
	const i = raw.indexOf(' ')
	return i <= 0
		? {id: raw, data: undefined}
		: {id: raw.substr(0, i), data: raw.substr(i + 1)}
}

module.exports = SocketServer
