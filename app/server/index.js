//@ts-check
'use strict'

const http = require('http')
const Express = require('express')
const config = require('../config')
const SocketServer = require('./SocketServer')
const ApiRouter = require('./ApiRouter')

/**
 * HTTP Server singleton instance
 * @type {http.Server | null | undefined}
 */
let server

/**
 * Starts the HTTP server
 * @returns {Promise<string>} Promise for server URL
 */
function startServer() {
	if (server !== undefined) {
		return Promise.reject(new Error('Server already starting/started'))
	}
	server = null

	// const httpsOptions = {
	// 	key: fs.readFileSync(path.join(__dirname, '..', '..', 'cert', 'key.pem')),
	// 	cert: fs.readFileSync(path.join(__dirname, '..', '..', 'cert', 'cert.pem'))
	// }

	const expressApp = Express()
	server = http.createServer(expressApp)
	// Handle JSON requests
	expressApp.use(Express.json())
	// Serve static files
	expressApp.use('/', Express.static(config.PUBLIC_WWW))
	// Setup API routes
	expressApp.use('/api', ApiRouter())

	// Setup socket server
	const socketServer = SocketServer(server)

	// Start listening on port...
	return new Promise((resolve, reject) => {
		server.listen(config.HTTP_PORT, err =>
			err ? reject(err) : resolve(`http://localhost:${config.HTTP_PORT}`)
		)
	})
}

/** Shuts down the HTTP server */
function stopServer() {
	if (server) {
		server.close(err => {
			server = undefined
			if (err) {
				console.error('Error shutting down HTTP server:', err)
			} else {
				console.log('HTTP server closed')
			}
		})
		server = null
	} else {
		console.warn('No HTTP server running')
	}
}

module.exports = {startServer, stopServer}
