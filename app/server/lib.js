//@ts-check
'use strict'

const path = require('path')
const fs = require('fs-extra')

/**
 * Filter a directory listing.
 * Uses lstat, so symlinks are not followed.
 * @param {string} directory
 * @param {(filename: string, stats: fs.Stats) => boolean} filter
 * @returns {Promise<string[]>}
 */
async function filterDirectory (directory, filter) {
	const filenames = await fs.readdir(directory)
	const stats = await Promise.all(
		filenames.map(
			filename => fs.lstat(path.join(directory, filename))
		)
	)
	return filenames.filter(
		(filename, i) => filter(filename, stats[i])
	)
}

module.exports = {filterDirectory}
