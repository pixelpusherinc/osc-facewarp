#version 300 es
precision highp float;

uniform vec2 uViewport;

in vec2 aPosition;
in vec2 aUv;

out vec2 vUv;

void main(void) {
	vUv = aUv;
	gl_Position = vec4(
		aPosition.x / uViewport.x, aPosition.y / uViewport.y, 0.5, 1.0
	);
}
