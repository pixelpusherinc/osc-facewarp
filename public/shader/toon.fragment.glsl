#version 300 es

precision highp float;

#define HueLevCount 6
#define SatLevCount 7
#define ValLevCount 4

const int NUM_SPOTS = 5; // Must be replaced after load

/**
 * X coordinate for side-by-side comparison.
 * (Set to > 1.0 to disable.)
 */
const float TEST_BORDER = 2.0;

const float uEdgeThres = 0.2;
const float uEdgeThres2 = 4.0;
//uniform float uEdgeThres;  // 0.2;
//uniform float uEdgeThres2; // 5.0;
uniform float uToon;
uniform float uSaturation;
uniform float uVignette;
uniform float uWarm;
uniform float uCool;
uniform sampler2D uTexture;
uniform vec4 uSpots[NUM_SPOTS];

in vec2 vUv;

out vec4 FragColor;

//float[HueLevCount] HueLevels = float[] (0.0, 140.0, 160.0, 240.0, 240.0, 360.0);
//float[SatLevCount] SatLevels = float[] (0.0, 0.15, 0.3, 0.45, 0.6, 0.8, 1.0);
//float[ValLevCount] ValLevels = float[] (0.0, 0.3, 0.6, 1.0);

float[HueLevCount] HueLevels = float[] (0.0, 10.0, 20.0, 30.0, 40.0, 50.0);
//float[HueLevCount] HueLevels = float[] (0.0, 80.0, 160.0, 240.0, 320.0, 360.0);
//float[HueLevCount] HueLevels = float[] (0.0, 72.0, 144.0, 216.0, 288.0, 360.0);
float[SatLevCount] SatLevels = float[] (0.0, 0.15, 0.3, 0.45, 0.6, 0.8, 1.0);
float[ValLevCount] ValLevels = float[] (0.0, 0.3, 0.6, 1.0);

vec3 rgbToHsv (float r, float g, float b) {
	float minv, maxv, delta;
	vec3 res;

	minv = min(min(r, g), b);
	maxv = max(max(r, g), b);
	res.z = maxv;            // v

	delta = maxv - minv;

	if (maxv != 0.0) {
		res.y = delta / maxv; // s
	} else {
		// r = g = b = 0      // s = 0, v is undefined
		res.y = 0.0;
		res.x = -1.0;
		return res;
	}

	if (r == maxv) {
		res.x = (g - b) / delta;       // between yellow & magenta
	} else if (g == maxv) {
		res.x = 2.0 + (b - r) / delta; // between cyan & yellow
	} else {
		res.x = 4.0 + (r - g) / delta; // between magenta & cyan
	}

	res.x = res.x * 60.0;  // degrees
	if (res.x < 0.0) {
		res.x = res.x + 360.0;
	}

	return res;
}

vec3 hsvToRgb (float h, float s, float v) {
	int i;
	float f, p, q, t;
	vec3 res;

	if (s == 0.0) {
		// achromatic (grey)
		res.x = v;
		res.y = v;
		res.z = v;
		return res;
	}

	h /= 60.0;          // sector 0 to 5
	i = int(floor(h));
	f = h - float(i);   // factorial part of h
	p = v * (1.0 - s);
	q = v * (1.0 - s * f);
	t = v * (1.0 - s * (1.0 - f));

	switch(i) {
		case 0:
			res.x = v;
			res.y = t;
			res.z = p;
			break;
		case 1:
			res.x = q;
			res.y = v;
			res.z = p;
			break;
		case 2:
			res.x = p;
			res.y = v;
			res.z = t;
			break;
		case 3:
			res.x = p;
			res.y = q;
			res.z = v;
			break;
		case 4:
			res.x = t;
			res.y = p;
			res.z = v;
			break;
		default:      // case 5:
			res.x = v;
			res.y = p;
			res.z = q;
			break;
	}
	return res;
}

float nearestLevel (float col, int mode) {
	int levCount;
	if (mode == 0) levCount = HueLevCount;
	if (mode == 1) levCount = SatLevCount;
	if (mode == 2) levCount = ValLevCount;

	for (int i = 0; i < levCount - 1; ++i) {
		if (mode == 0) {
			if (col >= HueLevels[i] && col <= HueLevels[i + 1]) {
				return HueLevels[i + 1];
			}
		}
		if (mode == 1) {
			if (col >= SatLevels[i] && col <= SatLevels[i + 1]) {
				return SatLevels[i + 1];
			}
		}
		if (mode == 2) {
			if (col >= ValLevels[i] && col <= ValLevels[i + 1]) {
				return ValLevels[i + 1];
			}
		}
	}
}

/** Averaged pixel intensity from 3 color channels */
float avgIntensity (vec4 pix) {
	return (pix.r + pix.g + pix.b) / 3.0;
}

vec4 getPixel (vec2 coords, float dx, float dy) {
	return texture(uTexture, coords + vec2(dx, dy));
}

float isEdge (in vec2 coords) {
	float dxtex = 1.0 / float(textureSize(uTexture, 0));
	float dytex = 1.0 / float(textureSize(uTexture, 0));
	float pix[9];
	int k = -1;
	float delta;

	// read neighboring pixel intensities
	for (int i = -1; i < 2; i++) {
		for(int j =-1; j < 2; j++) {
			k++;
			pix[k] = avgIntensity(
				getPixel(coords, float(i) * dxtex, float(j) * dytex)
			);
		}
	}

	// average color differences around neighboring pixels
	delta = (
		abs(pix[1] - pix[7])
		+ abs(pix[5] - pix[3])
		+ abs(pix[0] - pix[8])
		+ abs(pix[2] - pix[6])
	) / 4.0;

	// return clamp(5.5 * delta,0.0,1.0);
	return clamp(uEdgeThres2 * delta, 0.0, 1.0);
}

/**
 * Toon effect
 */
vec4 toon (vec2 uv) {
	vec4 tc = vec4(1.0, 0.0, 0.0, 1.0);
	if (uv.x < (TEST_BORDER - 0.002)) {
		vec3 colorOrg = texture(uTexture, uv).rgb;
		vec3 clrHsv = rgbToHsv(colorOrg.r, colorOrg.g, colorOrg.b);
		clrHsv.x = nearestLevel(clrHsv.x, 0);
		clrHsv.y = nearestLevel(clrHsv.y, 1);
		clrHsv.z = nearestLevel(clrHsv.z, 2);
		float edg = isEdge(uv);
		vec3 clrRgb = edg >= uEdgeThres
			// This is an edge pixel
			? vec3(0.0, 0.0, 0.0)
			// This is a fill color pixel
			// hsvToRgb(clrHsv.x, clrHsv.y, clrHsv.z)
			: mix(colorOrg, hsvToRgb(clrHsv.x, clrHsv.y, clrHsv.z), 0.5);
		tc = vec4(clrRgb.x, clrRgb.y, clrRgb.z, 1.0);
	}
	else if (uv.x > (TEST_BORDER + 0.002)) {
		tc = texture(uTexture, uv);
	}
	return tc;
}

/**
 * Vignette Effect
 */
float vignette (vec2 p, float amount) {
	float d = 1.0 * length(p - vec2(0.5, 0.5));
	float v = clamp(1.0 - d * amount, 0.0, 1.0);
	return v; // * v * v;
}

/**
 * @param color The original color
 * @param amount The amount to adjust the saturation of the color
 * @returns The resulting color
 *
 * @example
 * vec3 greyScale = czm_saturation(color, 0.0);
 * vec3 doubleSaturation = czm_saturation(color, 2.0);
 */
vec3 saturate (vec3 color, float amount) {
    // Algorithm from Chapter 16 of OpenGL Shading Language
    const vec3 w = vec3(0.2125, 0.7154, 0.0721);
    vec3 intensity = vec3(dot(color, w));
    return mix(intensity, color, amount);
}

/**
 * Colorize Effect (warm/cool)
 */
vec3 colorize (vec3 srcClr, vec3 dstClr) {
	float v = (srcClr.r + srcClr.g + srcClr.b) / 3.0;
	return mix(dstClr * v, srcClr, 0.5);
}

/**
 * Apply distortions.
 * @returns Resulting distorted texture sample position.
 */
vec2 distort(vec2 uv) {
	float distortion = 0.0;
	vec4 spot = vec4(0);
	// Find the distortion with the most effect (if any)
	for (int i = 0; i < NUM_SPOTS; ++i) {
		vec4 s = uSpots[i];
		vec2 dp = uv - s.xy;
		// How much distortion from this spot
		float dst = clamp(1.0 - length(dp) / s.z, 0.0, 1.0);
		dst *= s.w;
		if (dst > distortion) {
			distortion = dst;
			spot = s;
		}
	}
	vec2 p = uv;
	// Is there a distortion effect on this pixel?
	if (distortion > 0.0) {
		// Apply distortion
		vec2 dp = p - spot.xy;
		p = spot.xy + (dp / (distortion + 1.0));
	}
	return p; //texture(uTexture, p);
}

// Program entry point
void main() {
	vec2 uv = distort(vUv);
	vec4 c = uToon > 0.0 ? toon(uv) : texture(uTexture, uv);
	c.rgb = saturate(c.rgb, 1.0 + 2.0 * uSaturation);
	c.rgb = uVignette > 0.0 ? vignette(uv, uVignette) * c.rgb : c.rgb;
	c.rgb = uCool > 0.0 ? colorize(c.rgb, vec3(0.6, 0.75, 1.0)) : c.rgb;
	c.rgb = uWarm > 0.0 ? colorize(c.rgb, vec3(1.0, 0.675, 0.125)) : c.rgb;
	FragColor = c;
}
