#version 300 es

precision highp float;

// Number of distortion spots
const int NUM_SPOTS = {{NUM_SPOTS}}; // Must be replaced after load

// Texture to sample
uniform sampler2D uTexture;

// Position: xy, Radius: z, Unused: w
uniform vec4 uSpots[NUM_SPOTS];

in vec2 vUv;

out vec4 FragColor;

void main(void) {
	float distortion = 0.0;
	vec4 spot = vec4(0);
	// Find the distortion with the most effect (if any)
	for (int i = 0; i < NUM_SPOTS; ++i) {
		vec4 s = uSpots[i];
		vec2 dp = vUv - s.xy;
		// How much distortion from this spot
		float dst = clamp(1.0 - length(dp) / s.z, 0.0, 1.0);
		if (dst > distortion) {
			distortion = dst;
			spot = s;
		}
	}
	vec2 p = vUv;
	// Is there a distortion effect on this pixel?
	if (distortion > 0.0) {
		// Apply distortion
		vec2 dp = p - spot.xy;
		p = spot.xy + (dp / (distortion + 1.0));
	}
	FragColor = texture(uTexture, p);
}
