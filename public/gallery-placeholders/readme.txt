Put default gallery images in this folder.

Images must be:
* lower case letters and/or numbers ONLY in the filename
* .png (RGB) format
* perfectly square
* at least 256x256 pixels in size, but no larger than 2048x2048

Images will be displayed in alphabetical order by filename, up to a max of 20.
